Fwtester entrance file 
======================

The entrance file is fwtester.py.
This file simply imports the FwTester class, generate an instance with the config file as argument, and calls its run method.
The code is quite simple: ::

    #!/usr/bin/env /usr/bin/python3
    # -*- coding: utf-8 -*-
    
    from classes.FwTester import FwTester
    
    if __name__ == "__main__":
        tester = FwTester("config/config.yml")
        tester.run()
