.. FWTester documentation master file, created by
   sphinx-quickstart on Sun Apr 24 17:02:45 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to FWTester's documentation!
====================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   readme
   modules
   reference
  

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
