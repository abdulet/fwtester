About modules
=============
Fwtester runs every test by a set of modules which are loaded into client and server LXC containers once the program execution begins.
The modules system is very flexible, allowing layer from 2 to 7 modules in to 2 kind of modules:

1. Simple modules: This type of modules consists in a single file with a single class which runs all the required module logic.
2. Complex modules: This type consists in a folder with many files and classes. Giving the ability to build modules that runs complex tests.

It is possible to have more than one version of any module, so if there is any developed module that doesn't fit to specific needs, it is possible to write a new version and the system will load the latest one.
The developer is fully free to code whatever inside the modules.
Of course there are some small requirements to be plugable into the system, these requirements will be covered in below sections.

Each module is composed by tow parts a client and a server side module.
Server modules are loaded by the server container and are responsible to listen connections for the correct protocol and its parameters to each test returning back what is expecting the client side module. 
Client modules are loaded by the client container and are responsible to send the test connection for the correct protocol and its parameters to each test and executes the logic to proccess the server side module response.
The client and server modules are executed by its respective container.

The modules are located in the following paths:

- Client plugins: clientservercode/clientplugins
- Server plugins: clientservercode/serverplugins

The following sections describes the details of each module type.

Simple modules
--------------
This type of modules consist in a single file with a single class inside it, each module must be compliant with the following requirements:

1. It must define these exit codes: ::

    # Exit codes
    OK = 0
    ERROR = 1
    ERR_TIMEOUT = 2
    ERR_EXCEPTION = 255

2. An __init__ method, which defines some module specfications, must be defined with a logger type object (from lib/logger.py not the standard module) variable as the first non optional argument: ::

    def __init__(self, logger):
        self._logger = logger --> Defines the logger to be used into the module.
        self.LAYER = 3|7 --> Defines the IP layer where the module is intended to run. Currently only layer 3 is supported.
        self.PROTOCOL_NUMBER = 6\|17|... --> Defines the IP protocol number to use for layer 3 modules.
        self.APPLICATION = "" --> Defines the name of the application to use for layer 7 modules (http, https, dns, dhcp...).
        self.PRIORITY = 1 --> The priority of this version of the module the higest number is the one executed. So 2 has more priority than 1 and 100 has more priority than 2, and one of course :).

3. A run method which will be executed by the system when a test requires the module. This method must be defined as follows: ::

    def run(self, proto, srcip, dstip, srcport, dstport, searchfor):

Where:

- proto: Is the IP protocol number defined by the test and would be probably the same as the module
- srcip: Is the client side ip address
- dstip: Is the server side ip address
- srcport: Is the source port number
- dstport: Is the destination port number or an icmp_type:icmp_code string for the icmp protocol
- searchfor: Is a string containing these fields "protocol-srcip-dstip-srcport-dstport", this string is used as a token shared between client and server container plugins to be sure that the test is executed and is for the expected one, avoiding possible unexpected effects between tests. It is not mandatory for a plugin to use it.

There is not any other requirement, the coder is completly free to write its own code from here!

Complex modules
---------------
This type of modules are a python module, with only one requirement, there must be a class file called with the same name as the module folder. So if you want to develop a module to test mastodon connections, and you called the folder mastodon then it must be a file called mastodon.py inside it. So the relative path for mastodon.py would be mastodon/mastodon.py, in this way the module management system knows which file must be included to load the module. The mastodon.py file must to be compliant whit the requirements described for simple modules.

Take in mind that, as python module, it must comply with the python module requirements, basically to have an empty file called __init__.py.

And thats all, enjoy free coding!.
