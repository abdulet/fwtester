# -*- coding: utf-8 -*-
# Class to process each test line (access control rule)
import random

# Import loca libs
from lib.network import network

# Import local classes
from classes.Test import Test


class AccessControlRule():
    """
    Class to proccess each rule and convert it to unitary tests
    """

    def __init__(self, logger):
        logger.debug("AccessControlRule.__init__: BEGIN", 10)
        self.tests = []
        # Class property to store unsupported protocols in the rule
        self.unsupported_protocols = []
        self._logger = logger
        self._logger.debug("""AccessControlRule.__init__:
            Method Variables:
                self.tests: {self.tests!r}
                self.unsupported_protocols: {self.unsupported_protocols!r}
                self._logger: {self._logger!r}""".format(self=self), 80
                           )
        self._logger.debug("AccessControlRule.__init__: END", 10)

    def _get_addresses(self, addresses):
        self._logger.debug("AccessControlRule._get_addresses: BEGIN", 10)
        addresses = addresses.split(" ")
        return_addresses = addresses
        if self._one_test_per_line:
            # Avoids long test by running only one test per line
            # by skipping recursion in multiple src or dst rules
            return_addresses = []
            return_addresses.append(
                addresses[random.randint(0, len(addresses)-1)]
            )
            self._logger.debug("AccessControlRule._get_addresses: inside \
one_test_per_line if return_addresses = {f1}".
                               format(f1=repr(return_addresses)), 90
                               )
        self._logger.debug("""AccessControlRule._get_addresses:
            Method Variables:
                addresses: {f1}
                return_addresses: {f2}
                self._one_test_per_line: {f3}""".format(
            f1=repr(addresses),
            f2=repr(return_addresses),
            f3=repr(self._one_test_per_line)), 80
        )
        self._logger.debug("AccessControlRule._get_addresses: END", 10)
        return return_addresses

    def _get_applications(self, applications):
        self._logger.debug("AccessControlRule._get_applications: BEGIN", 10)
        applications = applications.split(" ")
        return_applications = applications
        if self._one_test_per_line:
            # Avoids long test by running only one test per line
            # by skipping recursion in multiple src or dst rules
            return_applications = []
            return_applications.append(
                applications[random.randint(0, len(applications)-1)]
            )
            self._logger.debug("AccessControlRule._get_applications: inside \
one_test_per_line if return_applications = {f1}".
                               format(f1=repr(return_applications)), 90
                               )
        self._logger.debug("""AccessControlRule._get_applications:
            Method Variables:
                applications: {f1}
                return_applications: {f2}
                self._one_test_per_line: {f3}""".format(
            f1=repr(applications),
            f2=repr(return_applications),
            f3=repr(self._one_test_per_line)), 80
        )
        self._logger.debug("AccessControlRule._get_applications: END", 10)
        return return_applications

    def _get_ports(self, ports, add_protocols=None):
        self._logger.debug("AccessControlRule._get_ports: BEGIN", 10)
        ports = ports.split(" ")
        return_ports = []
        if len(self._protocols) > 0 and add_protocols is not None:
            self._logger.debug(
                "AccessControlRule._get_ports: Protocols and ports loop.", 90)
            for protocol in self._protocols:
                for port in ports:
                    self._logger.debug(
                        "AccessControlRule._get_ports: protocol {f1}, \
port {f2}".format(f1=repr(protocol), f2=repr(port)), 90
                    )
                    return_ports.append(str(protocol)+":"+str(port))
        else:
            return_ports = ports
            self._logger.debug("AccessControlRule._get_ports: (match else \
part in protocols if) return_ports = {f1}".format(f1=repr(return_ports)), 90)
        if self._one_test_per_line:
            # Avoids long test by running only one test per line
            # by skipping recursion in multiple src or dst rules
            return_ports = [ports[random.randint(0, len(ports)-1)]]
            self._logger.debug("AccessControlRule._get_ports: (match if part \
in one_test_per_line if) return_ports = {f1}".format(
                f1=repr(return_ports)), 90)
        self._logger.debug("""AccessControlRule._get_ports:
            Method Variables:
                ports (parameter: {f1}
                return_ports: {f2}
                self._protocols: {f3}
                add_protocols (parameter): {f4}
                self._one_test_per_line: {f5}""".format(
            f1=repr(ports),
            f2=repr(return_ports),
            f3=repr(self._protocols),
            f4=repr(add_protocols),
            f5=repr(self._one_test_per_line)), 80
        )
        self._logger.debug("AccessControlRule._get_ports: END", 10)
        return return_ports

    def _get_rule_protocols(self, protocols, supported_protocols):
        self._logger.debug("AccessControlRule._get_rule_protocols: BEGIN", 10)
        return_protocols = []
        self._logger.debug(
            "AccessControlRule._get_rule_protocols: Protocols loop", 90)
        for proto in protocols.split(" "):
            if str(proto).lower() in ["ip", "any", ""]:
                self._logger.debug("AccessControlRule._get_rule_protocols: \
(match if part in any if) moving proto from {f1} to tcp".
                                   format(f1=repr(proto)), 90
                                   )
                proto = "tcp"
            self._logger.debug("AccessControlRule._get_rule_protocols: \
Protocol {f1}".format(f1=repr(proto)), 90)
            if str(proto).lower() not in supported_protocols:
                self._logger.error("AccessControlRule._get_rule_protocols: \
Unsupported protocol{f1}".format(f1=repr(proto)))
                self.unsupported_protocols.append(proto)
            else:
                return_protocols.append(proto)
        if self._one_test_per_line:
            # Avoids long test by running only one test per line
            # by skipping recursion in multiple src or dst rules
            return_protocols = [return_protocols[
                random.randint(0, len(return_protocols)-1)]]
            self._logger.debug("AccessControlRule._get_rule_protocols: \
(match if part in one_test_per_line if) return_protocols = {f1}".
                               format(f1=repr(return_protocols)), 90
                               )
        self._logger.debug("""AccessControlRule._get_rule_protocols:
            Method Variables:
                protocols (parameter): {f1}
                supported_protocols (parameter): {f2}
                return_protocols: {f3}
                self.unsupported_protocols: {f4}
                self._one_test_per_line: {f5}""".format(
            f1=repr(protocols),
            f2=repr(supported_protocols),
            f3=repr(return_protocols),
            f4=repr(self.unsupported_protocols),
            f5=repr(self._one_test_per_line)), 80
        )
        self._logger.debug("AccessControlRule._get_rule_protocols: END", 10)
        return return_protocols

    def get_rule_tests(self, rule, supported_protocols, one_test_per_line,
                       fw_nics, fw_routes, test_one_port_per_network_pair):
        """
        Method to split a rule into unitary tests
        """
        self._logger.debug("AccessControlRule.get_rule_tests: BEGIN", 10)

        # Variable init
        rule_number, action, protocols, sources, srcports, destinations,\
            dstports, applications, src_zone, dst_zone,\
            run_only_client_container = rule.split(",")
        self._one_test_per_line = one_test_per_line
        self._protocols = self._get_rule_protocols(
            protocols, supported_protocols)
        self._source_addresses = self._get_addresses(sources)
        self._destination_addresses = self._get_addresses(destinations)
        self._source_ports = self._get_ports(srcports)
        self._destination_ports = self._get_ports(dstports)
        self._applications = self._get_applications(applications)
        self._run_only_client_container = True
        if str(run_only_client_container).lower() in ["no", "false", ""]:
            self._run_only_client_container = False

        self._logger.debug(
            "AccessControlRule.get_rule_tests: Rule elements loop", 90)
        for source_address in self._source_addresses:
            net_object = network(self._logger)
            source_address = net_object.get_address(source_address, fw_nics,
                                                    fw_routes, src_zone)
            for destination_address in self._destination_addresses:
                destination_address = net_object.get_address(
                    destination_address, fw_nics, fw_routes, dst_zone)
                for source_port in self._source_ports:
                    for protocol in self._protocols:
                        try:
                            protocol, source_port = net_object.get_port(
                                source_port, protocol)
                        except Exception:
                            continue
                        for destination_port in self._destination_ports:
                            if test_one_port_per_network_pair:
                                destination_port = self._destination_ports[
                                    random.randint(
                                        0, len(self._destination_ports)-1)]
                                self._logger.debug(
                                    "AccessControlRule.get_rule_tests: (match \
if part in test_one_port_per_network_pair if) destination_port = {f1}"
                                    .format(f1=repr(destination_port)), 90
                                )
                            try:
                                protocol, destination_port = net_object\
                                    .get_port(destination_port, protocol)
                            except Exception:
                                continue
                            if len(applications) <= 0:
                                applications = [""]
                            for application in self._applications:
                                self.tests.append(Test(
                                    action, source_address, source_port,
                                    destination_address, destination_port,
                                    application, protocol, rule_number,
                                    self._run_only_client_container,
                                    self._logger)
                                )
                            self._logger.debug(
                                """AccessControlRule.get_rule_tests:
    source_address: {f1}
    destination_address: {f2}
    source_port: {f3}
    protocol: {f4}
    destination_port: {f5}
    application: {f6}
    run_only_client_container{f7}""".format(
                                    f1=repr(source_address),
                                    f2=repr(destination_address),
                                    f3=repr(source_port),
                                    f4=repr(protocol),
                                    f5=repr(destination_port),
                                    f6=repr(application),
                                    f7=repr(self._run_only_client_container)),
                                90)
                            if test_one_port_per_network_pair:
                                break
                        if test_one_port_per_network_pair:
                            break
                    if test_one_port_per_network_pair:
                        self._logger.debug("AccessControlRule: One port per \
network pair done.", 90)
                        break
        self._logger.debug("""AccessControlRule.get_rule_tests:
            Method Variables:
                rule (parameter): {f1}
                supported_protocols (parameter): {f2}
                one_test_per_line (parameter): {f3}
                fw_nics (parameter): {f4}
                fw_routes (parameter): {f5}
                test_one_port_per_network_pair (parameter): {f6}
                rule_number: {f7}
                action: {f8}
                protocols: {f9}
                sources: {f10}
                srcports: {f11}
                destinations: {f12}
                dstports: {f13}
                src_zone: {f14}
                dst_zone: {f15}
                self._one_test_per_line: {f16}
                self._protocols: {f17}
                self._source_addresses: {f18}
                self._destination_addresses: {f19}
                self._source_ports: {f20}
                self._destination_ports: {f21}""".format(
            f1=repr(rule),
            f2=repr(supported_protocols),
            f3=repr(one_test_per_line),
            f4=repr(fw_nics),
            f5=repr(fw_routes),
            f6=repr(test_one_port_per_network_pair),
            f7=repr(rule_number),
            f8=repr(action),
            f9=repr(protocols),
            f10=repr(sources),
            f11=repr(srcports),
            f12=repr(destinations),
            f13=repr(dstports),
            f14=repr(src_zone),
            f15=repr(dst_zone),
            f16=repr(self._one_test_per_line),
            f17=repr(self._protocols),
            f18=repr(self._source_addresses),
            f19=repr(self._destination_addresses),
            f20=repr(self._source_ports),
            f21=repr(self._destination_ports)), 80
        )
        self._protocols = []
        self._logger.debug("AccessControlRule.get_rule_tests: END", 10)
