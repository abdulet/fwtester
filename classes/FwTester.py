# -*- coding: utf-8 -*-
import fnmatch
import ipaddress
import logging
import os

# Local libraries
from lib.datastorage import datastorage
from lib.logger import logger
from lib.report import report
from lib.config import config

# Classes import
from classes.AccessControlRule import AccessControlRule
from classes.Container import Container
from classes.TestData import TestData


class FwTester():
    """Main class of the program, it's the brain of the code, its main purpose
 is to build and control the flow of the rest of classes and
 libraries."""

    def __init__(self, config_file):
        """Initializes the main objects used by the program through this class.
    Which are:
        self.one_test_per_line: Configuration parameter which limits the
 execution of one test per each firewall rule. The source, destination and port
 are randomly choosen from the ones in the rule.
        self._max_retries: Configuration parameter which limits maximum
 retries for a firewall rule, with a permit action, if it execution times out.
        self._test_one_port_per_network_pair: Config param which limits to one
 the number of ports to be tested for a pair of source and destination networks
 in a firewall rule. The port executed is randomly selected per each networks
 pair.
        self._max_tests_per_line: Configuration parameter which limits the
 number of tests to be executed per firewall rule.
        self._test_data: TestData type object used to import data for tests.
        self._client_container: Container type objecto to manage lxc client
 container.
        self._server_container: {f7}
        self._router_container: {f8}
        self._containers: {f9}
        self._supported_protocols: {f10}
        self._fw_nics: {f11}
        self._fw_vlans: {f12}
        self._fw_routes: {f13}
        self._raw_fw_nics: {f14}
        self._tests: {f15}
        self._vlans: {f16}
        self._used_vlans: {f17}
        self._tests_results: {f18}
        self._internal_storage: {f19}"""

        self._config = config(config_file)

        # Logger object to use over all the program
        self._logger = logger(
            "outputs/logs/fwtester.log", console_level=logging.ERROR,
            debug_level=self._config.params["debug_level"]
        )
        self._logger.debug("FWTester.__init__: BEGIN", 10)
        self._config.set_logger(self._logger)

        # General config vars
        # skip recursion in rules with any conbitaion of multiple:
        # sources, destinations, source port, destination port and protocols
        self.one_test_per_line = self._config.params["one_test_per_line"]
        self._max_retries = self._config.params["max_retries_for_timeouts"]
        self._test_one_port_per_network_pair = self._config.params[
            "test_one_port_per_network_pair"]
        self._max_tests_per_line = self._config.params["max_tests_per_line"]

        # Object responsible of importing the data used for the test
        self._test_data = TestData("csv", self._logger)

        # Report variables
        self._report = report(self._logger, "csv")

        # Containers ro run the tests
        # Containervariables
        self._client_container = Container("client", self._logger)
        self._server_container = Container("server", self._logger)
        self._router_container = Container("router", self._logger)
        self._containers = [
            self._client_container,
            self._server_container,
            self._router_container
        ]
        # Supported protocols
        self._supported_protocols = ["tcp", "udp", "icmp", "ip", ""]

        # Firewall networking configuration
        self._fw_nics = {}
        self._fw_vlans = {"byname": {}, "byid": []}
        self._fw_routes = self._test_data.get_fw_routes("./csvdata/routes.csv")
        self._raw_fw_nics = self._test_data.get_fw_nics("./csvdata/fwnics.csv")
        self._tests = self._test_data.get_tests("./csvdata/tests.csv")

        # Vlans used by FwTester
        self._vlans = {}
        self._used_vlans = []

        # List to store tests results
        self._tests_results = []

        # Data storage for internal use, stores the last test executed
        self._internal_storage = datastorage(
            "mongodb", "fwtester", self._logger)

        self._logger.debug("""AccessControlRule.__init__:
            Method Variables:
                self.one_test_per_line: {f1}
                self._max_retries: {f2}
                self._test_one_port_per_network_pair: {f3}
                self._max_tests_per_line: {f4}
                self._test_data: {f5}
                self._client_container: {f6}
                self._server_container: {f7}
                self._router_container: {f8}
                self._containers: {f9}
                self._supported_protocols: {f10}
                self._fw_nics: {f11}
                self._fw_vlans: {f12}
                self._fw_routes: {f13}
                self._raw_fw_nics: {f14}
                self._tests: {f15}
                self._vlans: {f16}
                self._used_vlans: {f17}
                self._tests_results: {f18}
                self._internal_storage: {f19}""".format(
            f1=repr(self.one_test_per_line),
            f2=repr(self._max_retries),
            f3=repr(self._test_one_port_per_network_pair),
            f4=repr(self._max_tests_per_line),
            f5=repr(self._test_data),
            f6=repr(self._client_container),
            f7=repr(self._server_container),
            f8=repr(self._router_container),
            f9=repr(self._containers),
            f10=repr(self._supported_protocols),
            f11=repr(self._fw_nics),
            f12=repr(self._fw_vlans),
            f13=repr(self._fw_routes),
            f14=repr(self._raw_fw_nics),
            f15=repr(self._tests),
            f16=repr(self._vlans),
            f17=repr(self._used_vlans),
            f18=repr(self._tests_results),
            f19=repr(self._internal_storage)), 80)
        self._logger.debug("FWTester.__init__: END", 10)

    def _add_clientside_nic_to_router(self, side):
        self._logger.debug("FWTester._add_clientside_nic_to_router: BEGIN", 10)
        device = self._ruletest.topology[str(side)]
        routertofwip = str(
            ipaddress.ip_address(device["ip"]))
        mask = device["mask"]
        src = self._ruletest.source_address
        # Defines ip addresses for the router as the client gateway
        routertoendpointmask = "30"
        self._logger.debug("FwTester._add_clientside_nic_to_router:\
 routertoendpointmask loop", 90)
        while str(src) == str(
                ipaddress.ip_interface(str(src)+"/"+str(routertoendpointmask)).
                network.network_address) or str(src) == str(
                ipaddress.ip_interface(str(src)+"/"+str(routertoendpointmask)).
                network.broadcast_address):
            routertoendpointmask = str(int(routertoendpointmask)-1)
            self._logger.debug("FwTester._add_clientside_nic_to_router:\
 routertoendpointmask = {f1}".format(f1=repr(routertoendpointmask)), 90)
        clientnet = ipaddress.ip_interface(
            str(src)+"/"+str(routertoendpointmask)).network
        if ipaddress.ip_address(str(src)) != clientnet[1]:
            # Use first ip of the network as gateway
            routerip = str(clientnet[1])
            self._logger.debug("FwTester._add_clientside_nic_to_router:\
 (match if part) routerip = {f1}".format(f1=repr(routerip)), 90)
        else:
            # Use the last ip of the network as gateway
            routerip = str(clientnet[-2])
            self._logger.debug("FwTester._add_clientside_nic_to_router:\
 (match else part) routerip = {f1}".format(f1=repr(routerip)), 90)
        routervlan = str(self._get_free_vlan())

        # Client side container: 1 NIC to the router
        # container
        self._client_container.add_interface(
            src, routertoendpointmask, routervlan
        )
        self._client_container.set_gateway(routerip)

        # Router container: client side
        # 1 NIC to the client container
        self._router_container.add_interface(
            routerip, routertoendpointmask, routervlan
        )

        # 1 NIC to the firewall
        self._router_container.add_interface(
            routertofwip, mask, device["vlan"]
        )

        # Setup policy routing rules
        self._router_container.add_policy_rule(
            self._ruletest.source_address,
            self._ruletest.topology["client"]["vlan"], "client"
        )
        self._router_container.add_policy_rule(
            self._ruletest.destination_address, routervlan, "client"
        )

        # Add default route to client table
        self._router_container.set_gateway(self._ruletest.gateway, "client")

        # Add client network's route to client table
        self._router_container.add_route(src, routertoendpointmask,
                                         routervlan, "client")

        # Add firewall network's route to client table
        self._router_container.add_route(routertofwip, mask,
                                         device["vlan"], "client")
        # Add server specific route to client table
        self._router_container.add_route(
            self._ruletest.destination_address, "32", self._ruletest.gateway,
            "client", True
        )
        self._logger.debug("""FWTester._add_clientside_nic_to_router:
            Method Variables:
                side: {f1}
                device: {f2}
                routertofwip: {f3}
                mask: {f4}
                src: {f5}
                routertoendpointmask: {f6}
                clientnet: {f7}
                routerip: {f8}
                routervlan: {f9}""".format(
            f1=repr(side),
            f2=repr(device),
            f3=repr(routertofwip),
            f4=repr(mask),
            f5=repr(src),
            f6=repr(clientnet),
            f7=repr(routerip),
            f8=repr(routerip),
            f9=repr(routervlan)
        ), 80)
        self._logger.debug("FWTester._add_clientside_nic_to_router: END", 10)

    def add_nics(self):
        self._logger.debug("FWTester.add_nics: BEGIN", 10)
        self._ruletest.get_topology(
            self._fw_nics, self._fw_routes
        )
        self._logger.debug("FWtester.add_nics: Netside loop.", 90)
        for netside in self._ruletest.topology:
            side = netside
            device = self._ruletest.topology[side]
            self._ruletest.gateway = None
            connectsto = device["connectsto"]
            mask = device["mask"]
            vlan = device["vlan"]
            self._logger.debug(
                "FwTester.add_nics: Device side: {f1}, connects to: \
{f2}, mask: {f3}, vlan: {f4}".
                format(f1=repr(side), f2=repr(connectsto), f3=repr(mask),
                       f4=repr(vlan)), 90)

            if connectsto == "firewall":
                # The net is directly connected to the firewall
                self._ruletest.gateway = device["ip"]
                self.add_nic_to_fw(side, mask)
                self._logger.debug("FwTester.add_nics: (match if part in \
connectsto if) self._ruletest.gateway = {f1}".format(
                    f1=repr(self._ruletest.gateway)))
            elif not self._ruletest.run_only_client_container:
                # The net connects to the firewall by an
                # intermediate router
                self._logger.debug("FwTester.add_nics: (match else part in\
 connectsto if)", 90)
                self._logger.debug("FWtester.add_nics: Nics loop", 90)
                for nic in self._fw_nics:
                    nic = self._fw_nics[nic]
                    self._logger.debug("FWtester.add_nics: nic = {f1}".format(
                        f1=repr(nic)), 90)
                    if int(nic["vlan"]) == int(vlan):
                        self._ruletest.gateway = nic["ip"]
                        self._logger.debug("FWtester.add_nics: (match if part\
 in vlan if) self._ruletest.gateway = {f1}".format(
                            f1=repr(self._ruletest.gateway)), 90)
                if self._ruletest.gateway is None:
                    self._ruletest.gateway = device["gw"]
                    self._logger.debug("FwTester.add_nics: (match if part in \
self._ruletest.gateway if) self._ruletest.gateway = {f1}".format(
                        f1=repr(self._ruletest.gateway)), 90
                    )
                self.add_nic_to_router(side)
        self._logger.debug("""FwTester.add_nics:
            Method Variables:
                self._ruletest.topology: {f1}""".format(
            f1=repr(self._ruletest.topology)), 80)

    def add_nic_to_fw(self, side, mask):
        self._logger.debug("FWTester.add_nic_to_fw: BEGIN", 10)
        if side == "client":
            # client side container: 1 NIC to the firewall
            self._logger.debug(
                "FwTester.runTest: Configuring client \
container NICs", 10)
            self._client_container.add_interface(
                self._ruletest.source_address, mask,
                self._ruletest.topology[str(side)]["vlan"]
            )
            self._client_container.set_gateway(self._ruletest.gateway)
        elif not self._ruletest.run_only_client_container:
            # server side container: 1 NIC to the firewall
            self._logger.debug("FwTester.runTest: \
Configuring server container NICs", 10)
            self._server_container.add_interface(
                self._ruletest.destination_address, mask,
                self._ruletest.topology[str(side)]["vlan"]
            )
            self._server_container.set_gateway(self._ruletest.gateway)
        self._logger.debug("""FwTester.add_nic_to_fw:
            Method Variables:
                side: {f1}
                mask: {f2}""".format(
            f1=repr(side),
            f2=repr(mask)), 80)
        self._logger.debug("FWTester.add_nic_to_fw: END", 10)

    def add_nic_to_router(self, side):
        self._logger.debug("FwTester.add_nic_to_router: BEGIN", 10)
        if side == "client":
            self._add_clientside_nic_to_router(side)
        else:
            self._add_serverside_nic_to_router(side)
        self._logger.debug("FwTester.add_nic_to_router: END", 10)

    def _add_serverside_nic_to_router(self, side):
        self._logger.debug("FWTester._add_serverside_nic_to_router: BEGIN", 10)
        device = self._ruletest.topology[str(side)]
        routertofwip = str(ipaddress.ip_address(device["ip"]))
        mask = device["mask"]
        dst = self._ruletest.destination_address
        # Defines ip addresses for the router as the server gateway
        routertoendpointmask = "30"
        if str(dst) == str(
                ipaddress.ip_interface(str(dst)+"/"+str(routertoendpointmask)).
                network.network_address) or str(dst) == str(
                ipaddress.ip_interface(str(dst)+"/"+str(routertoendpointmask)).
                network.broadcast_address):
            routertoendpointmask = str(int(routertoendpointmask)+1)
            self._logger.debug("FWtester._add_serverside_nic_to_router: (\
 match if) dst in network or bradcast addreess so add 1 to router to endpoint\
 mask {f1}".format(f1=repr(routertoendpointmask)), 90)
        servernet = ipaddress.ip_interface(
            str(dst)+"/"+str(routertoendpointmask)).network
        if ipaddress.ip_address(str(dst)) != servernet[1]:
            # Use first ip of the network as gateway
            routerip = str(servernet[1])
            self._logger.debug("FwTester._add_serverside_nic_to_router: (match\
 if part in dst if) routerip = {f1}".format(f1=repr(routerip)), 90)
        else:
            # Use the last ip of the network as gateway
            routerip = str(servernet[-2])
            self._logger.debug("FwTester._add_serverside_nic_to_router: (match\
 else part in dst if) routerip = {f1}".format(f1=repr(routerip)), 90)
        routervlan = str(self._get_free_vlan())

        # Server side container: 1 NIC to the router container
        self._server_container.add_interface(
            dst, routertoendpointmask, routervlan
        )

        self._server_container.set_gateway(routerip)

        # Router container: Server side
        # 1 NIC to the server container
        self._router_container.add_interface(
            routerip, routertoendpointmask, routervlan
        )

        # 1 NIC to the firewall
        self._router_container.add_interface(
            routertofwip, mask, device["vlan"])

        # Setup policy routing rules
        self._router_container.add_policy_rule(
            self._ruletest.destination_address,
            self._ruletest.topology["server"]["vlan"], "server"
        )
        self._router_container.add_policy_rule(
            self._ruletest.source_address, routervlan, "server"
        )

        # Add default route to server table
        self._router_container.set_gateway(self._ruletest.gateway, "server")

        # Add route to server network into server table
        self._router_container.add_route(dst, routertoendpointmask,
                                         routervlan, "server")
        # Add route to firewall network to server table
        self._router_container.add_route(routertofwip, mask,
                                         device["vlan"], "server")
        # Add client specific route into server table
        self._router_container.add_route(
            self._ruletest.source_address, "32", self._ruletest.gateway,
            "server", True
        )
        self._logger.debug("""FwTester._add_serverside_nic_to_router:
            Method Variables:
                side: {f1}
                device: {f2}
                routertofwip: {f3}
                mask: {f4}
                dst: {f5}
                routertoendpointmask: {f6}
                servernet: {f7}
                routerip: {f8}
                routervlan: {f9}""".format(
            f1=repr(side),
            f2=repr(device),
            f3=repr(routertofwip),
            f4=repr(mask),
            f5=repr(dst),
            f6=repr(routertoendpointmask),
            f7=repr(servernet),
            f8=repr(routerip),
            f9=repr(routervlan)), 80)
        self._logger.debug("FWTester._add_serverside_nic_to_router: END", 10)

    def configure_containers(self):
        self._logger.debug("FWTester.configure_containers: BEGIN", 10)
        self.add_nics()
        self._logger.debug("FWTester.configure_containers: END", 10)

    def _containers_flush(self):
        self._logger.debug("FWTester._containers_flush: BEGIN", 10)
        for container in self._containers:
            container.flush_network()
        self._used_vlans = []
        self._logger.debug("FWTester._containers_flush: END", 10)

    def containers_start(self):
        self._logger.debug("containers_start: BEGIN", 10)
        for container in self.containers:
            container.start(self._logger)
        self._logger.debug("containers_start: END", 10)

    def containers_stop(self):
        self._logger.debug("FWTester.containers_stop: BEGIN", 10)
        for container in self.containers:
            container.stop(self._logger)
        self._logger.debug("FWTester.containers_stop: END", 10)

    def generate_report(self):
        self._logger.debug("FWTester.generate_report: BEGIN", 10)
        headers = "Rule number, Action, Result, Source, Source port,\
Destination, Destination Port"
        self._report.add_section("Test Results", headers, self._tests_results)
        self._report.generate("outputs/reports/report")
        self._logger.debug("FWTester.generate_report: END", 10)

    def _get_containers_logs(self):
        # TODO: find a way to get this files
        self._logger.debug("FWTester._get_containers_logs: BEGIN", 10)
        return
        self._client_container.get_file(
            "client/root/clientsocket*.log", "outputs/logs")
        self._server_container.get_file(
            "server/root/serversocket*.log", "outputs/logs")
        try:
            self._client_container.get_file(
                "client/root/except*", "outputs/logs")
        except Exception:
            pass
        try:
            self._server_container.get_file(
                "server/root/except*", "outputs/logs")
        except Exception:
            pass
        self._logger.debug("FWTester.: END", 10)

    def _get_free_vlan(self):
        self._logger.debug("FWTester._get_free_vlan: BEGIN", 10)
        # sets vlanid to the maximum vlan accepted by linux or ubuntu not sure
        vlanid = 4094
        self._logger.debug("FWtester._get_free_vlan: vlanid loop", 90)
        while int(vlanid) > 0:
            self._logger.debug("FWtester._get_free_vlan: vlanid",)
            if vlanid not in self._fw_vlans["byid"]+self._used_vlans:
                self._used_vlans.append(int(vlanid))
                break
            vlanid -= 1
        if vlanid == 0:
            self._logger.error(
                "getfreevlan: Can't find a free vlan id for the router")
            raise ValueError(
                "getfreevlan", "ERROR", "Can't find a free vlan for the router"
            )
        self._logger.debug("""FwTester._get_free_vlan:
            Method Variables:
                vlanid: {f1}""".format(f1=repr(vlanid)), 80)
        self._logger.debug("FWTester._get_free_vlan: END", 10)
        return vlanid

    def _get_fw_nics(self):
        # Proccess al FW NICs in a dict
        # to be used in other functions
        self._logger.debug("FwTester.get_fwnics: BEGIN", 10)
        self._logger.debug("FWtester.get_fwnics: nics loop", 90)
        for nic in self._raw_fw_nics:
            self._logger.debug("FWtester.get_fwnics: nic = {f1}".format(
                f1=repr(nic)), 90)
            nic = nic.split(",")
            self._fw_nics[str(nic[0]).lower()] = {
                "ip": nic[1],
                "mask": nic[2],
                "vlan": nic[3]
            }
        self._logger.debug("""FwTester.get_fwnics:
            Method Variables:
                self._fw_nics: {f1}""".format(f1=repr(self._fw_nics)), 80)
        self._logger.debug("FwTester.get_fwnics: END", 10)

    def _get_fw_vlans(self):
        # Put all firewall vlans in a list
        # to be used in other functions
        self._logger.debug("FwTester._get_fw_vlans: BEGIN", 10)
        self._logger.debug("FWtester._get_fw_vlans: nics loop", 90)
        for nic in self._fw_nics:
            self._logger.debug("FWtester._get_fw_vlans: nic = {f1}".format(
                f1=repr(nic)), 90)
            self._fw_vlans["byname"][
                str(self._fw_nics[nic]["vlan"])] = nic
            self._fw_vlans["byid"].append(self._fw_nics[nic]["vlan"])
        self._logger.debug("""FwTester._get_fw_vlans:
            Method Variables:
                self._fw_vlans: {f1}""".format(f1=repr(self._fw_vlans)), 80)
        self._logger.debug("FwTester._get_fw_vlans: END", 10)

    def _import_test_data(self):
        self._logger.debug("FWTester._import_test_data: BEGIN", 10)
        self._raw_fwnics = None
        self._logger.debug("FWTester._import_test_data: END", 10)

    def run(self):
        self._logger.debug("FwTester.run: BEGIN", 10)
        """Main method which manage all logic"""
        self.run_tests()
        self.generate_report()
        self._logger.debug("FwTester.run: END", 10)

    def run_tests(self):
        self._logger.debug("FwTester.run_test: BEGIN", 10)
        self._get_fw_nics()
        self._get_fw_vlans()

        self._server_container.push_file(
            "clientservercode/Server.py", "/root/")
        for path, dirs, files in os.walk("clientservercode/serverplugins/"):
            for file in fnmatch.filter(files, "*.py"):
                self._server_container.push_file(
                    "clientservercode/serverplugins/"+str(file),
                    "/root/serverplugins/"+str(file))
        self._client_container.push_file(
            "clientservercode/Client.py", "/root/")
        for path, dirs, files in os.walk("clientservercode/clientplugins/"):
            for file in fnmatch.filter(files, "*.py"):
                self._client_container.push_file(
                    "clientservercode/clientplugins/"+str(file),
                    "/root/clientplugins/"+str(file))
        last_test = self._resume()
        self._logger.debug("FWtester.run_tests: tests lines loop.", 90)
        for test in self._tests:
            i = 0
            self._logger.debug(
                "FwTester.run_test: running test line: {f1}".format(
                    f1=repr(test)), 90
            )
            ruletests = AccessControlRule(self._logger)
            self._logger.debug(
                "FwTester.run_test: ruletests = {f1}".format(
                    f1=repr(ruletests)), 90
            )
            ruletests.get_rule_tests(
                test, self._supported_protocols, self.one_test_per_line,
                self._fw_nics, self._fw_routes,
                self._test_one_port_per_network_pair
            )
            self._logger.debug("FWtester.run_tests: line tests loop.", 90)
            for ruletest in ruletests.tests:
                self._logger.debug(
                    "FwTester.run_test: running line test: {f1}".format(
                        f1=repr(ruletest.rule_number)), 90)
                if self._max_tests_per_line > 0 and\
                        i >= self._max_tests_per_line:
                    self._logger.debug(
                        "FwTester.run: Max tests per line limit reached {f1}."
                        .format(f1=repr(self._max_tests_per_line)), 90)
                    break
                i += 1
                if int(ruletest.rule_number) <= int(last_test):
                    # Skip to porccess already done rules
                    self._logger.debug(
                        "FwTester.run_test: Skipping test {f1} becasue resume"
                        .format(f1=ruletest.rule_number), 90
                    )
                    continue
                # Adds required NICs to containers to run the test
                self._ruletest = ruletest
                self.configure_containers()
                self._logger.debug(
                    "FwTester.run_test: self._ruletest = {f1}".format(
                        f1=repr(self._ruletest)), 90
                )
                # run the test
                self._tests_results.append(ruletest.run(
                    self._client_container, self._server_container,
                    self._max_retries)
                )
                # Cleans the containers configuration
                self._containers_flush()
                self._save_status(self._tests_results[-1])
                self._get_containers_logs()
        self._logger.debug("""FwTester.run_test:
            Method Variables:
                last_test: {f1}""".format(f1=repr(last_test)), 80)
        self._logger.debug("FWTester.run_test: END", 10)

    def _save_status(self, row):
        self._logger.debug("BEGIN: _save_status")
        row = row.split(",")
        row = {
            "rule_number": row[0],
            "action": row[1],
            "result": row[2],
            "protocol": row[3],
            "source_address": row[4],
            "source_port": row[5],
            "destination_address": row[6],
            "destination_port": row[7]
        }
        self._internal_storage.add_row(row, "tests_results")
        self._logger.debug("""FwTester._save_status:
            Method Variables:
                row: {f1}""".format(f1=repr(row)), 80)
        self._logger.debug("END: _save_status")

    def _resume(self):
        self._logger.debug("BEGIN: resume")
        results = self._internal_storage.get_rows("tests_results")
        last_rule_done = 0
        if len(results) > 0:
            last_rule_done = int(results[-1]["rule_number"])
        self._logger.debug("""FwTester.:
            Method Variables:
                results: {f1}
                last_rule_done: {f2}""".format(
            f1=repr(results),
            f2=repr(last_rule_done)), 80)
        self._logger.debug("END: resume")
        return last_rule_done

    def show_usage(self):
        self._logger.debug("FWTester.show_usage: BEGIN", 10)
        self._logger.debug("FWTester.show_usage: END", 10)
        pass
