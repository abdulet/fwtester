# -*- coding: utf-8 -*-
import ipaddress
import pexpect


class Test():
    """Class to process each test line"""

    def __init__(
            self, action,
            source, source_port,
            destination, destination_port, application,
            protocol, rule_number, run_only_client_container, logger):
        self._logger = logger
        self._logger.debug("Test.__init__: BEGIN", 10)
        self.action = action
        self.source_address = source
        self.source_port = source_port
        self.application = application
        self.destination_address = destination
        self.destination_port = destination_port
        self.protocol = protocol
        self.rule_number = rule_number
        self.run_only_client_container = run_only_client_container
        self.vlans = {}
        self.gateway = None
        self._logger.debug("""Test.__init__:
            Method Variables:
                action: {f1}
                source: {f2}
                source_port: {f3}
                destination: {f4}
                destination_port: {f5}
                application: {f10}
                protocol: {f6}
                rule_number: {f7}
                run_only_client_container: {f11}
                self.vlans: {f8}
                self.gateway: {f9}""".format(
            f1=repr(action),
            f2=repr(source),
            f3=repr(source_port),
            f4=repr(destination),
            f5=repr(destination_port),
            f6=repr(protocol),
            f7=repr(rule_number),
            f8=repr(self.vlans),
            f9=repr(self.gateway),
            f10=repr(application),
            f11=repr(run_only_client_container)), 80
        )
        self._logger.debug("Test.__init__: END", 10)

    def get_connections_to_fw(self, fw_nics):
        """Gets topology for devices which connects directly to FW"""
        self._logger.debug("Test.get_connections_to_fw: BEGIN", 10)
        self._logger.debug("Test.get_connections_to_fw: fw_nics loop", 90)
        for nic in fw_nics:
            interface = nic
            nic = fw_nics[nic]
            ip = nic["ip"]

            # Gets the mask in CIDR notation
            mask = str(ipaddress.ip_network(ipaddress.ip_interface(
                str(ip+"/"+nic["mask"])).network).prefixlen)
            network = ipaddress.ip_interface(
                str(ip+"/"+mask)).network
            vlan = nic["vlan"]
            nic["mask"] = mask
            self.vlans[interface] = vlan
            self._logger.debug(
                f"""Test.get_connections_to_fw: insidefor variables:
    interface: {interface}
    nic: {nic}
    ip: {ip}
    mask: {mask}
    network: {network}
    vlan: {vlan}
    nic["mask"]: {nic["mask"]}
    self.vlans[interface]: {self.vlans[interface]}
    self.source_address: {self.source_address}
    self.destination_address: {self.destination_address}
    """
            )

            if self.source_address in network:
                self._logger.debug(
                    "Test.get_connections_to_fw: Client is directly connected \
to FW NIC: IP: {f1}, mask: {f2}, vlan: {f3}".format(
                        f1=repr(ip), f2=repr(mask), f3=repr(vlan)), 90
                )
                self.topology["client"] = {
                    "connectsto": "firewall", "ip": ip, "mask": mask,
                    "vlan": vlan
                }
            if self.destination_address in network:
                self._logger.debug(
                    "Test.get_connections_to_fw: Server is directly connected \
to FW NIC: IP: {f1}, mask: {f2}, vlan: {f3}".format(
                        f1=repr(ip), f2=repr(mask), f3=repr(vlan)), 90
                )
                self.topology["server"] = {
                    "connectsto": "firewall", "ip": ip, "mask": mask,
                    "vlan": vlan
                }
        self._logger.debug("""Test.get_connections_to_fw:
            Method Variables:
                fw_nics: {f1}""".format(f1=repr(fw_nics)), 80
                           )
        self._logger.debug("Test.get_connections_to_fw: END", 10)

    def get_intermediate_routers(self, fw_nics, fw_routes):
        """Gets topology for devices which connects through
         an intermediate router"""
        self._logger.debug("Test.get_intermediate_routers: BEGIN", 10)
        if not self.topology["client"] or not self.topology["server"]:
            # Check if targets needs an intermediate router
            self._logger.debug(
                "Test.get_intermediate_routers: (inside topology if) Searching\
 fw_routes for intermediate router", 90)
            self._logger.debug(
                "Test.get_intermediate_routers:fw_routes loop", 90)
            default_route = None
            for route in fw_routes:
                net = route.replace("\n", "").split(",")
                interface = str(net[0]).lower()
                ip = net[1]

                # Ensure to get the mask in CIDR notation
                mask = str(ipaddress.ip_network(ipaddress.ip_interface(
                    str(ip+"/"+net[2])).network).prefixlen)
                network = ipaddress.ip_interface(str(ip+"/"+mask)).network
                gateway = net[3]
                # metric = net[4] # TODO: Add support to metric
                if str(network) == "0.0.0.0/0":
                    default_route = {"connectsto": "router",
                                     "ip": gateway,
                                     "mask": fw_nics[interface]["mask"],
                                     "vlan": self.vlans[interface]
                                     }

                if self.source_address in network:
                    if not self.topology["client"]:
                        self._logger.debug(
                            "Test.get_intermediate_routers: Source network \
match network: {f1} for route {f2}".format(f1=repr(network), f2=repr(route)),
                            90)
                        self._logger.debug(
                            "Test.get_intermediate_routers: Client connects to\
 a router. FW NIC: IP: {f1}, mask: {f2}, vlan: {f3}".format(
                                f1=repr(gateway),
                                f2=repr(fw_nics[interface]["mask"]),
                                f3=repr(self.vlans[interface])), 90
                        )
                        self.topology["client"] = {
                            "connectsto": "router",
                            "ip": gateway,
                            "mask": fw_nics[interface]["mask"],
                            "vlan": self.vlans[interface],
                            "routemask": mask
                        }
                    else:
                        # To get the most restrictive route
                        self._logger.debug(
                            "Test.get_intermediate_routers: Route mask: {f1}, \
previous route mask: {f2}".format(
                                f1=repr(mask),
                                f2=repr(self.topology["client"]["mask"])), 90
                        )
                        if "routemask" not in self.topology["client"]:
                            self.topology["client"]["routemask"] =\
                                self.topology["client"]["mask"]
                        if int(mask) > int(
                                self.topology["client"]["routemask"]):
                            self._logger.debug(
                                "Test.get_intermediate_routers: Source network\
 match a more restrictive route: {f1} for route {f2}".format(
                                    f1=repr(network), f2=repr(route)), 90
                            )
                            self._logger.debug(
                                "Test.get_intermediate_routers: Client \
connects to a router. FW NIC: IP: {f1}, mask: {f2}, vlan: {f3}".format(
                                    f1=repr(gateway),
                                    f2=repr(fw_nics[interface]["mask"]),
                                    f3=repr(self.vlans[interface])), 90
                            )
                            self.topology["client"].update({
                                "connectsto": "router",
                                "ip": gateway,
                                "mask": fw_nics[interface]["mask"],
                                "vlan": self.vlans[interface],
                                "routemask": mask}
                            )

                if self.destination_address in network:
                    if not self.topology["server"]:
                        self._logger.debug(
                            "Test.get_intermediate_routers: Destination \
network match network: {f1} for route {f2}".format(
                                f1=repr(network), f2=repr(route)), 90
                        )
                        self._logger.debug(
                            "Test.get_intermediate_routers: Server connects to\
 a router. FW NIC: {f1}, mask: {f2}, vlan: {f3}".format(
                                f1=repr(gateway),
                                f2=repr(fw_nics[interface]["mask"]),
                                f3=repr(self.vlans[interface])), 90
                        )
                        self.topology["server"] = {
                            "connectsto": "router",
                            "ip": gateway,
                            "mask": fw_nics[interface]["mask"],
                            "vlan": self.vlans[interface],
                            "routemask": mask
                        }
                    else:
                        # To get the most restrictive route
                        if "routemask" not in self.topology["server"]:
                            self.topology["server"]["routemask"] =\
                                self.topology["server"]["mask"]
                        if int(mask) > int(
                                self.topology["server"]["routemask"]):
                            self._logger.debug(
                                "Test.get_intermediate_routers: Destination \
network match a more restrictive route: {f1} for route {f2}".format
                                (f1=repr(network), f2=repr(route)), 90
                            )
                            self._logger.debug(
                                "Test.get_intermediate_routers: Server \
connects to a router. FW NIC: {f1}, mask: {f2}, vlan: {f3}".format(
                                    f1=repr(gateway),
                                    f2=repr(fw_nics[interface]["mask"]),
                                    f3=repr(self.vlans[interface])), 90
                            )
                            self.topology["server"].update({
                                "connectsto": "router",
                                "ip": gateway,
                                "mask": fw_nics[interface]["mask"],
                                "vlan": self.vlans[interface],
                                "routemask": mask}
                            )

            if not self.topology["client"] and default_route is not None:
                # There is not specific route to reach client.
                # So use default one
                self._logger.debug(
                    "Test.get_intermediate_routers: Source network match \
default route {f1}".format(f1=repr(default_route)), 90
                )
                self.topology["client"].update(default_route)

            if not self.topology["server"] and default_route is not None:
                # There is not specific route to reach server.
                # So use default one
                self._logger.debug(
                    "Test.get_intermediate_routers: Destination network match \
default route {f1}".format(f1=repr(default_route)), 90
                )
                self.topology["server"].update(default_route)
        if not self.topology["client"]:
            self._logger.error(
                "Test.get_intermediate_routers: Route to client not found")
        elif "routemask" in self.topology["client"]:
            del (self.topology["client"]["routemask"])
        if not self.topology["server"]:
            self._logger.error(
                "Test.get_intermediate_routers: Route to server not found")
        elif "routemask" in self.topology["server"]:
            del (self.topology["server"]["routemask"])
        self._logger.debug("""Test.:
            Method Variables:
                fw_nics: {f1}
                fw_routes: {f2}
                self.topology["client"]: {f4}
                self.topology["server"]: {f5}""".format(
            f1=repr(fw_nics),
            f2=repr(fw_routes),
            f4=repr(self.topology["client"]),
            f5=repr(self.topology["server"])), 80
        )
        self._logger.debug("Test.get_intermediate_routers: END", 10)

    def get_topology(self, fw_nics, fw_routes):
        self._logger.debug("Test.get_topology: BEGIN", 10)
        self._logger.debug(
            "Test.get_topology: Getting topology for targets: {f1}".format(
                f1=repr([self.source_address, self.destination_address])), 90
        )
        self._logger.debug(
            "Test.get_topology: self.source_address = {f1}, \
self.destination_address = {f2}".format(
                f1=repr(self.source_address),
                f2=repr(self.destination_address)), 90
        )
        self.source_address = ipaddress.ip_address(
            str(self.source_address))
        self.destination_address = ipaddress.ip_address(
            str(self.destination_address))
        self.topology = {"client": False, "server": False}

        # Gets topology of source and destination if directly connected to FW
        self.get_connections_to_fw(fw_nics)
        # Gets topology of source and destination if connected to router
        self.get_intermediate_routers(fw_nics, fw_routes)
        self._logger.debug("""Test.get_topology:
            Method Variables:
                fw_nics: {f1}
                fw_routes: {f2}
                self.source_address: {f3}
                self.destination_address: {f4}""".format(
            f1=repr(fw_nics),
            f2=repr(fw_routes),
            f3=repr(self.source_address),
            f4=repr(self.destination_address)
        ), 80
        )
        self._logger.debug("Test.get_topology: END", 10)

    def run(self, client_container, server_container, max_retries=3):
        """Run the test"""
        self._logger.debug("Test.run: BEGIN", 10)
        srvconn = None
        searchfor = "-".join([
            str(self.protocol), str(self.source_address),
            str(self.source_port), str(self.destination_address),
            str(self.destination_port)]
        )
        if not self.run_only_client_container:
            self._logger.debug("Test.run: Starting server socket", 10)
            args = [
                "exec", server_container.name, "--", "python",
                "/root/Server.py", "-proto", str(self.protocol),
                "-srcip", str(self.source_address),
                "-dstip", str(self.destination_address),
                "-sp", str(self.source_port),
                "-dp", str(self.destination_port),
                "-search", searchfor]
            if self.application not in [None, ""]:
                args.append("-app")
                args.append(self.application)
            srvconn = server_container.run_interactive_command("lxc", args)
            self._logger.debug("Test.run: Server running", 10)
        self._logger.debug("Test.run: Starting client socket", 10)
        retries = 1
        self._logger.debug("Test.run: retries loop", 90)
        while retries <= max_retries:
            self._logger.debug("Test.run: Running test try {f1}".
                               format(f1=repr(retries)), 90)
            args = [
                "exec", str(client_container.name), "--", "python",
                "/root/Client.py",
                "-proto", str(self.protocol),
                "-sp", str(self.source_port),
                "-srcip", str(self.source_address),
                "-dstip", str(self.destination_address),
                "-dp", str(self.destination_port),
                "-search", searchfor]
            if str(self.application) not in [None, ""]:
                args.append("-app")
                args.append(self.application)
            conn = client_container.run_interactive_command("lxc", args)
            try:
                conn.expect(["OK.*", "FAIL.*"])
                result = conn.after.decode(encoding='UTF-8')
                self._logger.info("Test.run: Result {!s}".format(result))
                result = str(result).replace("\n", "")
                result = str(result).replace("\r", "")
                self._logger.debug(f"Test.run: result var is {result}")
                if result.lower() == "ok":
                    if self.action.lower() in\
                            ["accept", "allow", "permit"]:
                        result = "ok"
                    else:
                        self._logger.debug(
                            "Test.run: Failed in accept if, action {f1}"
                            .format(f1=repr(self.action)), 90
                        )
                        result = "fail"
                elif result.lower() == "fail":
                    if self.action.lower() in\
                            ["accept", "allow", "permit"]:
                        self._logger.debug("Test.run: Failed in fail if", 90)
                        result = "fail"
                    else:
                        result = "ok"
                if result == "ok":
                    # Result is ok so break try loop
                    break
            except (pexpect.exceptions.EOF, pexpect.exceptions.TIMEOUT):
                if self.action.lower() in\
                        ["accept", "allow", "permit"]:
                    if retries == max_retries:
                        self._logger.debug(
                            "Test.run: Failed in timeout except", 90)
                        result = "fail"
                else:
                    result = "ok"
                    # Result is ok so break try loop
                    break
            retries += 1

        if result.lower() == "fail" and self._logger.debug_level >= 100:
            self._logger.debug("Failed test so exit for debug.")
            print("Failed test so exit for debug.")
            exit(0)
        result = ",".join([
            self.rule_number, self.action, result]+searchfor.split("-"))
        self._logger.debug(
            "Test.run: Test result is: {f1}".format(f1=repr(result)), 10)
        self._logger.debug("""Test.run:
            Method Variables:
                client_container: {f1}
                server_container: {f2}
                max_retries: {f3}
                searchfor: {f4}
                result: {f5}""".format(
            f1=repr(client_container),
            f2=repr(server_container),
            f3=repr(max_retries),
            f4=repr(searchfor),
            f5=repr(result)
        ), 80
        )
        del (conn)
        if srvconn is not None:
            del (srvconn)
        self._logger.debug("Test.run: END", 10)
        return result
