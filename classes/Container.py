# -*- coding: utf-8 -*-
import ipaddress
import pexpect
from subprocess import call


class Container:
    "Class used for lxc containers management"

    def __init__(self, container_name, logger):
        self._logger = logger
        self._logger.debug("Container.__init__: BEGIN", 10)
        self.name = container_name
        self.interfaces = []
        self.routes = []
        self.vlans = []
        self._running = False
        self._logger.debug("""Container.__init__:
            Method Variables:
                self.name: {f1}
                self.interfaces: {f2}
                self.routes: {f3}
                self.vlans: {f4}
                self._logger: {f5}
                self._running: {f6}""".format(
                f1=repr(self.name),
                f2=repr(self.interfaces),
                f3=repr(self.routes),
                f4=repr(self.vlans),
                f5=repr(self._logger),
                f6=repr(self._running)), 80
        )
        self._logger.debug("Container.__init__: END", 10)

    def add_interface(self, ip, mask, vlan, bridge_physical_nic="eth0"):
        self._logger.debug("Container.add_interface: BEGIN", 10)
        add_link_command = [
            "lxc", "exec", self.name, "--", "ip", "link", "add", "link",
            bridge_physical_nic, "name", "vlan"+str(vlan), "type", "vlan",
            "id", str(vlan)
            ]
        self.run_command(add_link_command)
        add_address_command = [
            "lxc", "exec", self.name, "--", "ip", "add", "add",
            str(ip)+"/"+str(mask), "dev", "vlan"+str(vlan)
            ]
        self.run_command(add_address_command)
        set_link_up_command = [
            "lxc", "exec", self.name, "--", "ip", "link", "set",
            "vlan"+str(vlan), "up"
            ]
        self.run_command(set_link_up_command)
        self.interfaces.append(vlan)
        self._logger.debug("""Container.add_interface:
            Method Variables:
                ip: {f1}
                maks: {f2}
                vlan: {f3}
                bridge_physical_nic: {f4}
                add_link_command: {f5}
                add_address_command: {f6}
                set_link_up_command: {f7}
                self.interfaces: {f8}""".format(
                f1=repr(ip),
                f2=repr(mask),
                f3=repr(vlan),
                f4=repr(bridge_physical_nic),
                f5=repr(add_link_command),
                f6=repr(add_address_command),
                f7=repr(set_link_up_command),
                f8=repr(self.interfaces)), 80
        )
        self._logger.debug("Container.add_interface: END", 10)

    def add_route(self, ip, mask, gw, table=None, dst_is_ip=None):
        self._logger.debug("Container.add_route: BEGIN", 10)
        if dst_is_ip is None:
            command = [
                "lxc", "exec", self.name, "--", "ip", "route", "add",
                str(ipaddress.ip_interface(str(ip)+"/"+str(mask)).network),
                "dev", "vlan"+str(gw)
                ]
            self._logger.debug("Container.add_route: (match if part) command = \
{f1}".format(f1=repr(command)), 90)
        else:
            command = [
                "lxc", "exec", self.name, "--", "ip", "route", "add",
                str(ipaddress.ip_interface(str(ip)+"/"+str(mask)).network),
                "via", str(gw)
                ]
            self._logger.debug("Container.add_route: (match else part) command = \
{f1}".format(f1=repr(command)), 90)
        if table is not None:
            command.append("table")
            command.append(str(table))
        self.run_command(command)
        self._logger.debug("""Container.add_route:
            Method Variables:
                ip: {f1}
                mask: {f2}
                gw: {f3}
                table: {f4}
                dst_is_ip: {f5}
                command: {f6}""".format(
                f1=repr(ip),
                f2=repr(mask),
                f3=repr(gw),
                f4=repr(table),
                f5=repr(dst_is_ip),
                f6=repr(command)
            ), 80
        )
        self._logger.debug("Container.add_route: END", 10)

    def add_policy_rule(self, address, vlan, table):
        self._logger.debug("Container.add_policy_rule: BEGIN", 10)
        command = [
            "lxc", "exec", self.name, "--", "ip", "rule", "add", "to",
            str(address), "iif", "vlan"+str(vlan), "table", str(table)
            ]
        self.run_command(command)
        self._logger.debug("""Container.add_policy_rule:
            Method Variables:
                address: {f1}
                vlan: {f2}
                table: {f3}
                command: {f4}""".format(
                f1=repr(address),
                f2=repr(vlan),
                f3=repr(table),
                f4=repr(command)), 80
        )
        self._logger.debug("Container.add_policy_rule: END", 10)

    def check_command(self, command):
        return True
        self._logger.debug("Container.check_command: BEGIN", 10)
        # TODO check if command is a valid lxc one
        if type(command) is not list:
            try:
                command = command.split(" ")
            except Exception:
                raise ValueError("Cannont split command {f1}".format(
                    f1=repr(command)))
        if command[0] != "lxc":
            raise ValueError("1st argument isn't lxc")
        if command[1] not in ["file", "restart", "start", "exec", "stop"]:
            raise ValueError("2nd argument isn't a valid lxc command")
            # TODO: finish all lxc commands
        if (command[1] == "file" and command[2] not in
                ["push", "pull", "delete"]):
            raise ValueError(
                "3rd argument isn't a valid lxc command in {f1}".format(
                    f1=repr(command))
            )
            # TODO: finish all lxc subcommands
        self._logger.debug("""Container.check_command:
            Method Variables:
                command: {f1}
                return: True""".format(
                f1=repr(command)), 80
        )
        self._logger.debug("Container.check_command: END", 10)
        return True

    def delete_file(self, file_path):
        self._logger.debug("Container.delete_file: BEGIN", 10)
        command = [
            "lxc", "file", "delete", self.name+str(file_path)
            ]
        self.run_command(command)
        self._logger.debug("""Container.check_command:
            Method Variables:
                command: {f1}""".format(
                f1=repr(command)), 80
        )
        self._logger.debug("Container.delete_file: END", 10)

    def flush_network(self):
        self._logger.debug("Container.flush_network: BEGIN", 10)
        self._logger.debug("Container.flush_network: interfaces loop", 90)
        for interface in self.interfaces:
            # Removes all container's NICs
            self._logger.debug(
                "Container.flush_network: Removing interface {f1}".format(
                    f1=repr(interface))
            )
            command = [
                "lxc", "exec", str(self.name), "--", "ip", "link",
                "del", "vlan"+str(interface)
                ]
            self._logger.debug("""Container.flush_network:
    interface: {f1}
    command: {f2}""".format(f1=repr(interface), f2=repr(command)), 90
            )
            self.run_command(command)
        self.interfaces = []

        if self.name.lower() == "router":
            self._logger.debug("Container.flush_network: tables loop", 90)
            for table in ["client", "server"]:
                # Removes all policy base routing
                self._logger.debug(
                    "Container.flush_network: Rules in table {f1}"
                    .format(f1=repr(table))
                )
                command = [
                    "lxc", "exec", str(self.name), "--", "ip", "rule", "flush",
                    "table", table
                ]
                self._logger.debug("""Container.flush_network:
    table: {f1}
    command: {f2}""".format(f1=repr(table), f2=repr(command)), 90
                )
                self.run_command(command)

        self._logger.debug("Container.flush_network: END", 10)

    def get_file(self, srcpath, dstpath):
        self._logger.debug("Container.get_file: BEGIN", 10)
        command = [
            "lxc", "file", "pull", self.name+str(srcpath), str(dstpath)
            ]
        self.run_command(command)
        self._logger.debug("""Container.get_file:
            Method Variables:
                srcpath: {f1}
                dstpath: {f2}""".format(
                f1=repr(srcpath),
                f2=repr(dstpath)), 80
        )
        self._logger.debug("Container.get_file: END", 10)

    def is_running(self):
        self._logger.debug("Container.is_running: BEGIN", 10)
        self._logger.debug("""Container.is_running:
            Method Variables:
                self._running: {f1}
            """.format(f1=repr(self._running)), 80
        )
        self._logger.debug("Container.is_running: END", 10)
        return self._running

    def push_file(self, srcpath, dstpath):
        self._logger.debug("Container.push_file: BEGIN", 10)
        command = [
            "lxc", "file", "push", str(srcpath), self.name+str(dstpath)
            ]
        self.run_command(command)
        self._logger.debug("""Container.push_file:
            Method Variables:
                srcpath: {f1}
                dstpath: {f2}
                command: {f3}""".format(
                f1=repr(srcpath),
                f2=repr(dstpath),
                f3=repr(command)), 80
        )
        self._logger.debug("Container.push_file: END", 10)

    def run_command(self, command):
        self._logger.debug("Container.run_command: BEGIN", 10)
        if self.check_command(command):
            call(command)
            self._logger.debug(
                "Container.run_command: Executed command {f1}".format(
                    f1=repr(" ".join(command))), 10)
        self._logger.debug("""Container.run_command:
            Method Variables:
                command: {f1}""".format(f1=repr(command)), 80)
        self._logger.debug("Container.run_command: END", 10)

    def run_interactive_command(self, command, params):
        self._logger.debug("Container.run_interactive_command: BEGIN", 10)
        if self.check_command([command]+params):
            self._logger.debug(" ".join([command]+params))
        self._logger.debug("""Container.run_interactive_command:
            Method Variables:
                command: {f1}
                params: {f2}""".format(
                f1=repr(command),
                f2=repr(params)), 80
        )
        self._logger.debug("Container.run_interactive_command: END", 10)
        return pexpect.spawn(command, params)

    def set_gateway(self, gateway, table=None):
        self._logger.debug("Container.set_gateway: BEGIN", 10)
        command = [
            "lxc", "exec", self.name, "--", "ip", "route", "replace",
            "default", "via", str(gateway)]
        if table is not None:
            command.append("table")
            command.append(str(table))
        self.run_command(command)
        self._logger.debug("""Container.set_gateway:
            Method Variables:
                gateway: {f1}
                table: {f2}
                command: {f3}""".format(
                f1=repr(gateway),
                f2=repr(table),
                f3=repr(command)), 80
        )
        self._logger.debug("Container.set_gateway: END", 10)

    def start(self):
        self._logger.debug("Container.start: BEGIN", 10)
        self.run_command(["lxc", "start", self.name])
        self._running = True
        self._logger.debug("Container.start: END", 10)

    def stop(self):
        self._logger.debug("Container.stop: BEGIN", 10)
        self.run_command(["lxc", "stop", self.name])
        self._running = False
        self._logger.debug("Container.start: END", 10)
