# -*- coding: utf-8 -*-
from lib.datastorage import datastorage


class TestData():
    "Interface class for data management classes"

    def __init__(self, data_storage_type, logger, storage=None):
        self._logger = logger
        self._logger.debug("TestData.__init__: BEGIN", 10)
        self.data_storage_type = data_storage_type.lower()
        self.data_storage = datastorage(
            self.data_storage_type, storage, self._logger
        )
        self._logger.debug("""TestData.__init__:
            Method Variables:
            data_storage_type: {f1}
            storage: {f2}
            self.data_storage_type: {f3}
            self.data_storage: {f4}""".format(
            f1=repr(data_storage_type),
            f2=repr(storage),
            f3=repr(self.data_storage_type),
            f4=repr(self.data_storage)), 80
        )
        self._logger.debug("TestData.__init__: END", 10)

    def get_fw_nics(self, table):
        self._logger.debug("TestData.get_fw_nics: BEGIN", 10)
        result = self.data_storage.get_rows(table)
        self._logger.debug("""TestData.get_fw_nics:
            Method Variables:
                result: {f1}""".format(f1=repr(result)
                                       ), 80
                           )
        self._logger.debug("TestData.get_fw_nics: END", 10)
        return result

    def get_fw_routes(self, table):
        self._logger.debug("TestData.get_fw_routes: BEGIN", 10)
        result = self.data_storage.get_rows(table)
        self._logger.debug("""TestData.get_fw_routes:
            Method Variables:
                result: {f1}""".format(f1=repr(result)
                                       ), 80
                           )
        self._logger.debug("TestData.get_fw_routes: END", 10)
        return result

    def get_tests(self, table):
        self._logger.debug("TestData.get_tests: BEGIN", 10)
        result = self.data_storage.get_rows(table)
        self._logger.debug("""TestData.get_tests:
            Method Variables:
                result: {f1}""".format(f1=repr(result)
                                       ), 80
                           )
        self._logger.debug("TestData.get_tests: END", 10)
        return result
