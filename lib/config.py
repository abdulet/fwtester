# -*- coding: utf-8 -*-
import yaml


class config():
    """Class to manage configuration files in yml format"""
    def __init__(self, path):
        self._path = path
        with open(str(path), "r") as yaml_file:
            self.params = yaml.load(yaml_file, Loader=yaml.SafeLoader)
        try:
            with open(str(path), "r") as yaml_file:
                self.params = yaml.load(yaml_file, Loader=yaml.SafeLoader)
        except:
            raise Exception ("config._init_: File not found {f1}".
                    format(f1=repr(path)))

    def set_logger(self, logger):
        self._logger = logger
        self._logger.debug("config.set_logger: BEGIN", 10)
        self._logger.debug("config.set_logger: END", 10)


    def get_parameter(self, name):
        self._logger.debug("config.get_parameter: BEGIN", 10)
        result = None
        if name in self.params:
            result = self.params[name]
        self._logger.debug("""config.:
            Method Variables:
                name: {f1}
                self.params[name]: {f2}""".format(
                f1=repr(name),
                f2=repr(self.params[name])
                )
            , 80
        )
        self._logger.debug("config.get_parameter: END", 10)
        return result

    def set_parameter(self, name, value):
        self._logger.debug("config.set_parameter: BEGIN", 10)
        self.params[name] = value
        self._logger.debug("""config.:
            Method Variables:
                name: {f1}
                value: {f2}
                self.params[name]: {f3}""".format(
                f1=repr(name),
                f2=repr(value),
                f3=repr(self.params[name])
                )
            , 80
        )
        self._logger.debug("config.set_parameter: END", 10)

    def save(self, path=None):
        self._logger.debug("config.save: BEGIN", 10)
        if path is None:
            path = self._path
        yaml.dump(self.params, path)
        self._logger.debug("""config.save:
            Method Variables:
                path: {f1}
                self.params: {f2}""".format(
                f1=repr(path),
                f2=repr(self.params)
                )
            , 80
        )
        self._logger.debug("config.save: END", 10)
