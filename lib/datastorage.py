# -*- coding: utf-8 -*-
from lib.datastoragecsv import datastoragecsv
from lib.datastoragesqlite import datastoragesqlite
from lib.datastoragemongodb import datastoragemongodb


class datastorage():
    """Class to abstract access to different type of datastorages"""

    def __init__(
            self, driver, storage, logger, username=None, password=None,
            host=None):
        self._logger = logger
        self._logger.debug("datastorage.__init__: BEGIN", 10)
        self.driver = driver.lower()
        if self.driver == "csv":
            self.storage = datastoragecsv(self._logger)
        elif self.driver == "sqlite":
            self.storage = datastoragesqlite()
        elif self.driver == "mongodb":
            self.storage = datastoragemongodb(storage, self._logger)
        self._logger.debug("""datastorage.__init__:
            Method Variables:
                driver: {f1}
                storage: {f2}
                username: {f3}
                password: {f4}
                host: {f5}""".format(
                f1=repr(driver),
                f2=repr(storage),
                f3=repr(username),
                f4=repr(password),
                f5=repr(host))
            , 80
        )
        self._logger.debug("datastorage.__init__: END", 10)

    def add_row(self, row, table):
        self._logger.debug("datastorage.add_row: BEGIN", 10)
        self.storage.add_row(row, table)
        self._logger.debug("""datastorage.add_row:
            Method Variables:
                row: {f1}
                table: {f2}""".format(
                f1=repr(row),
                f2=repr(table))
            , 80
        )
        self._logger.debug("datastorage.add_row: END", 10)

    def add_rows(self, rows, table):
        self._logger.debug("datastorage.add_rows: BEGIN", 10)
        self.storage.add_rows(rows, table)
        self._logger.debug("""datastorage.add_rows:
            Method Variables:
                rows: {f1}
                table: {f2}""".format(
                f1=repr(rows),
                f2=repr(table)
                )
            , 80
        )
        self._logger.debug("datastorage.add_rows: END", 10)

    def get_rows(self, table, num_of_rows=None, offset=0,
                 headers_in_file=True):
        self._logger.debug("datastorage.get_rows: BEGIN", 10)
        if self.driver == "csv":
            result = self.storage.get_rows(
                table, num_of_rows, offset, headers_in_file
            )
        else:
            result = self.storage.get_rows(
                table, num_of_rows, offset
            )
        self._logger.debug("datastorage.get_rows: Rows retrieved begin", 100)
        self._logger.debug(repr(result), 100)
        self._logger.debug("datastorage.get_rows: Rows retrieved end", 100)
        self._logger.debug("""datastorage.get_rows:
            Method Variables:
                table: {f1}
                num_of_rows: {f2}
                offset: {f3}
                headers_in_file: {f4}""".format(
                f1=repr(table),
                f2=repr(num_of_rows),
                f3=repr(offset),
                f4=repr(headers_in_file)
                )
            , 80
        )
        self._logger.debug("datastorage.get_rows: END", 10)
        return result
