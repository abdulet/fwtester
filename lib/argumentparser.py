# -*- coding: utf-8 -*-
# import argparse
from lib.argument import argument


class argumentparser():
    def __init__(self, logger):
        self._logger = logger
        self._logger.debug("argumentparser.__init__: BEGIN", 10)
        self._arguments = {}  # Dict of Argument class elements
        self._logger.debug("""argumentparser.__init__:
            Method Variables:
                self._arguments: {f1}""".format(f1=repr(self._arguments))
            , 80
        )
        self._logger.debug("argumentparser.__init__: END", 10)

    def add_argument(self, argument_name, argument_default_value,
                     argument_helpstring, required=False):
        self._logger.debug("argumentparser.add_argument: BEGIN", 10)
        self._arguments[argument_name] = argument(
            argument_name, argument_default_value, argument_helpstring, False)
        self._logger.debug("""argumentparser.add_argument:
            Method Variables:
                argument_name: {f1}
                argument_default_value: {f2}
                argument_helpstring: {f3}
                required: {f4}
                self._arguments[argument_name]: {f5}""".format(
                f1=repr(argument_name),
                f2=repr(argument_default_value),
                f3=repr(argument_helpstring),
                f4=repr(required),
                f5=repr(self._arguments[argument_name])
                )
            , 80
        )
        self._logger.debug("argumentparser.add_argument: END", 10)

    def get_argument(self, argument_name):
        self._logger.debug("argumentparser.get_arguments: BEGIN", 10)
        result = None
        if self.is_defined(argument_name):
            result = self._arguments[argument_name]
        self._logger.debug("""argumentparser.get_argument:
            Method Variables:
                argument_name: {f1}
                result: {f2}""".format(
                f1=repr(argument_name),
                f2=repr(result)
                )
            , 80
        )
        self._logger.debug("argumentparser.get_arguments: END", 10)
        return result

    def get_arguments(self):
        self._logger.debug("argumentparser.get_arguments: BEGIN", 10)
        result = self._arguments
        self._logger.debug("""argumentparser.get_arguments:
            Method Variables:
                result: {f1}""".format(f1=repr(result))
            , 80
        )
        self._logger.debug("argumentparser.get_arguments: END", 10)
        return result

    def is_defined(self, argument_name):
        self._logger.debug("argumentparser.is_defined: BEGIN", 10)
        result = False
        if str(argument_name) in self._arguments:
            result = True
        self._logger.debug("""argumentparser.is_defined:
            Method Variables:
                argument_name: {f1}
                result: {f2}""".format(
                f1=repr(argument_name),
                f2=repr(result))
            , 80
        )
        self._logger.debug("argumentparser.is_defined: END", 10)
        return result

    def parse_arguments(self):
        #TODO: Write code
        self._logger.debug("argumentparser.parse_arguments: BEGIN", 10)
        self._logger.debug("argumentparser.parse_arguments: END", 10)
        pass
