# -*- coding: utf-8 -*-
from lib.reportcsv import reportcsv
from lib.reporthtml import reporthtml


class report():
    """
    Class responsible to write final reports reports.
    Currently it supports this formats:
    -CSV
    Its planned to support this others:
    -HTML
    -XML
    """

    def __init__(self, logger, report_type="csv"):
        self._logger = logger
        self._logger.debug("report.__init__: BEGIN", 10)
        self.report_type = report_type.lower()
        if self.report_type == "csv":
            self._report = reportcsv(self._logger)
        elif self.report_type == "html":
            self._report = reporthtml(self._logger)
        self._logger.debug("""report.__init__:
            Method Variables:
                report_type: {f1}
                self.report_type: {f2}
                self._report: {f3}""".format(
                f1=repr(report_type),
                f2=repr(self.report_type),
                f3=repr(self._report))
            , 80
        )
        self._logger.debug("report.__init__: END", 10)

    def add_section(self, section_name, section_headers, section_data):
        self._logger.debug("report.add_section: BEGIN", 10)
        self._report.add_section(section_name, section_headers, section_data)
        self._logger.debug("report.add_section: END", 10)

    def generate(self, report_name):
        self._logger.debug("report.generate: BEGIN", 10)
        self._report.generate(report_name)
        self._logger.debug("report.generate: END", 10)
