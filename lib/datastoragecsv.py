# -*- coding: utf-8 -*-
import codecs
from lib.datastorageinterface import datastorageinterface


class datastoragecsv(datastorageinterface):
    """Class to manage data storage of type csv"""

    def __init__(self, logger):
        self._logger = logger
        self._logger.debug("datastoragecsv.__init__: BEGIN", 10)
        self._logger.debug("datastoragecsv.__init__: END", 10)

    def add_row(self, row, table):
        self._logger.debug("datastoragecsv.add_row: BEGIN", 10)
        mode = "a"
        file_descriptor = codecs.open(table, mode, "utf-8")
        file_descriptor.write(row+"\n")
        file_descriptor.close()
        self._logger.debug("""datastoragecsv.add_row:
            Method Variables:
                row: {f1}
                table: {f2}
                mode: {f3}""".format(
            f1=repr(row),
            f2=repr(table),
            f3=repr(mode)), 80
        )
        self._logger.debug("datastoragecsv.add_row: END", 10)

    def add_rows(self, rows, table, truncate=False):
        self._logger.debug("datastoragecsv.add_rows: BEGIN", 10)
        mode = "a"
        if truncate:
            mode = "w"
        file_descriptor = codecs.open(table, mode, "utf-8")
        for row in rows:
            file_descriptor.write(row+"\n")
        file_descriptor.close()
        self._logger.debug("""datastoragecsv.add_rows:
            Method Variables:
                rows: {f1}
                table: {f2}
                mode: {f3}
                truncate: {f4}""".format(
            f1=repr(row),
            f2=repr(table),
            f3=repr(mode),
            f4=repr(truncate)), 80
        )
        self._logger.debug("datastoragecsv.add_rows: END", 10)

    def get_rows(
            self, table, num_of_rows=None, offset=0, headers_in_file=True):
        """
        @get_rows: read csv lines from a file and retun them as a tupple
        """
        self._logger.debug("datastoragecsv.get_rows: BEGIN", 10)
        file_descriptor = codecs.open(table, "r", "utf-8")
        lines = []
        header = False
        iteration = 1
        for line in file_descriptor.readlines():
            if not header and headers_in_file:
                header = True
                continue
            iteration += 1
            if line.find("#") > -1:
                # Remove comments from line
                line = line[:line.index("#")]
                if line == "":
                    # Line is a comment so continue to next one
                    continue
            if (num_of_rows is None or len(lines) <= num_of_rows) and\
                    (offset == 0 or iteration >= offset):
                lines.append(line.replace("\n", ""))
        self._logger.debug(
            "datastoragecsv.get_rows: Rows retrieved begin", 100)
        self._logger.debug(repr(lines), 100)
        self._logger.debug("datastoragecsv.get_rows: Rows retrieved end", 100)
        file_descriptor.close()
        self._logger.debug("datastoragecsv.get_rows: END", 10)
        self._logger.debug("""datastoragecsv.get_rows:
            Method Variables:
                table: {f1}
                num_of_rows: {f2}
                offset: {f3}
                headers_in_file: {f4}""".format(
            f1=repr(table),
            f2=repr(num_of_rows),
            f3=repr(offset),
            f4=repr(headers_in_file)), 80
        )
        return lines
