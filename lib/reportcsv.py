# -*- coding: utf-8 -*-
import codecs
from time import strftime

# Import local libs
from lib.reportinterface import reportinterface


class reportcsv(reportinterface):
    def __init__(self, logger):
        self._logger = logger
        self._logger.debug("reportcsv.__init__: BEGIN", 10)
        self.datetime = strftime("%Y_%m_%d-%H_%M_%S")
        self.sections = []
        self._logger.debug("""reportcsv.__init__:
            Method Variables:
                self.datetime: {f1}
                self.sections: {f2}""".format(
                f1=repr(self.datetime),
                f2=repr(self.sections))
            , 80
        )
        self._logger.debug("reportcsv.__init__: END", 10)

    def add_section(self, section_name, section_headers, section_data):
        self._logger.debug("reportcsv.add_section: BEGIN", 10)
        self.sections.append({
            "name": section_name,
            "headers": section_headers,
            "data": section_data
        })
        self._logger.debug("""reportcsv.add_section: report data is\
    {f1}""".format(f1=repr(section_data)), 100)
        self._logger.debug("""reportcsv.add_section:
            Method Variables:
                section_name: {f1}
                section_headers: {f2}""".format(
                f1=repr(section_name),
                f2=repr(section_headers))
            , 80
        )
        self._logger.debug("reportcsv.add_section: END", 10)

    def del_section(self):
        pass

    def generate(self, report_name):
        self._logger.debug("reportcsv.generate: BEGIN", 10)
        file_descriptor = codecs.open(
            report_name+"-"+self.datetime+".csv", "w", "utf-8")
        for section in self.sections:
            file_descriptor.write(section["name"]+"\n"+section["headers"]+"\n")
            file_descriptor.write("\n".join(section["data"]))
        self._logger.debug("""reportcsv.generate:
            Method Variables:
                report_name: {f1}""".format(f1=repr(report_name)) , 80
        )
        self._logger.debug("reportcsv.generate: END", 10)
