# -*- coding: utf-8 -*-
from abc import ABC, abstractmethod


class reportinterface(ABC):
    """Interface class for subclasses which manage reports"""

    @abstractmethod
    def add_section(self, section_name, section_headers, section_data):
        pass

    @abstractmethod
    def del_section(self, section_name):
        pass

    @abstractmethod
    def generate(self, report_name):
        pass
