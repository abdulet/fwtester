# -*- coding: utf-8 -*-
import ipaddress
import random
import socket
from faker import Faker


class network():
    """Class used to manage TCP/IP operations:
    -IP network and address
    -TCP/UPD Ports
    """

    def __init__(self, logger):
        self._logger = logger
        self._logger.debug("network.__init__: BEGIN", 10)
        self._logger.debug("network.__init__: END", 10)

    def _is_fw_address(self, address, fw_nics):
        # TODO: This function goes to Network class
        self._logger.debug("network._is_fw_address: BEGIN", 10)
        result = False
        for nic in fw_nics:
            if str(address) == str(fw_nics[nic]["ip"]):
                result = True
        self._logger.debug("""network._is_fw_address:
            Method Variables:
                address: {f1}
                fw_nics: {f2}
                result: {f3}""".format(
                f1=repr(address),
                f2=repr(fw_nics),
                f3=repr(result)),
                80
            )
        self._logger.debug("network._is_fw_address: END", 10)
        return result

    def _exclude_subnets(self, net, fw_nics, fw_routes, nic):
        """Search if the network is supernet of any other route or fw NIC.
           If yes, it exclude the subnet from the supernet and returns a random
           portion of not overlaped network.
        """
        # TODO: This function goes to Network class
        self._logger.debug("network._exclude_subnets: BEGIN", 10)
        ret = None
        self._logger.debug("network._exclude_subnets: Initial network is {f1}".
                           format(f1=str(net)), 90)
        network = ipaddress.ip_network(str(net))
        # Check if is supernet of any route
        for route in fw_routes:
            route = route.split(",")
            route_net = ipaddress.ip_network(str(route[1]+"/"+route[2]))
            if str(nic).lower() != str(route[0]).lower() and\
               network.supernet_of(route_net):
                self._logger.debug(
                 "network._exclude_subnets: Excluding subnet {f1} from route: \
{f2}".
                 format(f1=repr(route_net), f2=repr(route)), 90)
                try:
                    ret = list(network.address_exclude(route_net))
                except ValueError:
                    continue
        # Check if is supernet of any NIC
        for fwnic in fw_nics:
            if nic in fwnic:
                # Avoids to check the route NIC
                continue
            fwnic_net = ipaddress.ip_network(ipaddress.ip_interface(
                str(fw_nics[nic]["ip"])+"/" + str(fw_nics[nic]["mask"])).
                network)
            if network.supernet_of(fwnic_net):
                self._logger.debug(
                 "network._exclude_subnets: Excluding subnet {f1} from nic: \
{f2}".
                 format(f1=repr(fwnic_net), f2=repr(fwnic)), 90)
                try:
                    ret = list(network.address_exclude(fwnic_net))
                except ValueError:
                    continue
        if ret is not None and len(ret) == 0:
            ret = None
        self._logger.debug("network._exclude_subnets: END", 10)
        return ret

    def get_address(self, address, fw_nics, fw_routes, nic=""):
        # TODO: This function goes to Network class
        self._logger.debug("network.get_address: BEGIN", 10)
        ret = None
        nic = str(nic).lower()

        self._logger.debug(
            "network.get_address: Initial address is {f1}".format(
                f1=repr(address)), 90)
        if str(address).lower() in ["any", ""] and str(nic).lower() in fw_nics:
            routes_to_nic = []
            self._logger.debug(
                "network.get_address: nic {f1} in fw_nics and address is any.".
                format(f1=repr(nic)), 90)
            self._logger.debug("network.get_address: fw_router loop.", 90)
            for route in fw_routes:
                self._logger.debug(
                    "network.get_address: Processing route {f1} for any."
                            .format(f1=repr(route)), 90
                )
                net = route.split(",")
                if str(nic).lower() == str(net[0]).lower():
                    # Adds the route as a eligible one as any for this address
                    routes_to_nic.append(str(net[1]) + "/" + str(net[2]))
                    self._logger.debug(
                        "network.get_address: Adding route {f1} as valid for\
 any.".format(f1=repr(route)), 90
                    )

            if len(routes_to_nic) <= 1:
                if str(nic).lower() in fw_nics:
                    # Adds fw nic network as address for any
                    address = str(fw_nics[nic]["ip"])+"/" + \
                            str(fw_nics[nic]["mask"])
                    self._logger.debug(
                        "network.get_address: Gets nic {f1} with values {f2}\
as any.".format(f1=repr(nic), f2=repr(fw_nics[nic])), 90
                    )
            else:
                # Gets a random network from suitable routes for this address
                address = str(
                        routes_to_nic[random.randint(0, len(routes_to_nic)-1)])
                # Checks if selected address is a supernet of other route or
                # fw NIC, and runs until it gets one that it's not
                subnets = True
                while subnets is not None:
                    subnets = self._exclude_subnets(
                            address, fw_nics, fw_routes, nic)
                    if subnets is not None:
                        address = str(
                                subnets[random.randint(0, len(subnets)-1)])

                self._logger.debug(
                    "network.get_address: Gets {f1} randomly as any, from {f2}\
 nic routes.".format(f1=repr(address), f2=repr(nic)), 90
                )

                if address in ["0.0.0.0/0.0.0.0", "0/0"]:
                    # Sets address as public to get it from Faker
                    self._logger.debug("network.get_address: Address is \
0.0.0.0 so set as public to get it from Faker.", 90)
                    address = "public"

        while str(address).lower() in ["any", "", "public"]:
            self._logger.debug("network.get_address: any or public loop", 90)
            auxaddr = ipaddress.ip_address(str(Faker().ipv4()))
            self._logger.debug(
                "network.get_address: Faker address is {f1}".format(
                    f1=repr(auxaddr)), 90)
            if (not auxaddr.is_reserved
                    and not auxaddr.is_multicast
                    and not auxaddr.is_unspecified
                    and not auxaddr.is_loopback
                    and not auxaddr.is_link_local) or \
                    (str(address) == "public" and auxaddr.is_global):
                address = str(auxaddr)
                self._logger.debug("network.get_address: Valid Faker address \
found {f1}".format(f1=repr(address)), 90)
        try:
            ip, mask = address.split("/")
        except ValueError:
            mask = ""

        try:
            if str(mask) in ["255.255.255.255", "32"]:
                ret = str(ipaddress.ip_address(str(ip)))
            else:
                ret = str(ipaddress.ip_address(str(address)))
            self._logger.debug("network.get_address: Is a host address")
        except Exception:
            try:
                network = ipaddress.ip_interface(str(address)).network
                hosts = list(ipaddress.ip_network(network).hosts())
            # self._logger.debug("Network.get_address: Is a network address
# hosts in network: {f1}".format(f1=repr(hosts)))
                ret = None
                while ret is None or self._is_fw_address(ret, fw_nics):
                    ret = str(hosts[random.randint(0, len(hosts) - 1)])
                self._logger.debug("network.get_address: Selected random\
 address from network is {f1}".format(f1=repr(ret)), 90)
            except ValueError:
                self._logger.debug(
                    "network.get_address: Invalid address: {f1}"
                    .format(f1=repr(address))
                )
                raise ValueError(
                    "getaddress", "ERROR", "Invalid address: {f1}"
                    .format(f1=repr(address)))
        self._logger.debug("""network.get_address:
            Method Variables:
                address: {f1}
                fw_nics: {f2}
                fw_routes: {f3}
                nic: {f4}
                ret: {f5}""".format(
                f1=repr(address),
                f2=repr(fw_nics),
                f3=repr(fw_routes),
                f4=repr(nic),
                f5=repr(ret)),
            80
        )
        self._logger.debug("network.get_address: END", 10)
        return str(ret)

    def get_default_route(self, routes):
        for route in routes:
            route = route.replace("\n", "")
            if str(route[1]) == "0.0.0.0":
                return {"interface": route[0], "gw": route[3]}
        return None

    def get_mask(self, network):
        self._logger.debug("network.get_mask: BEGIN", 10)
        self._logger.debug("network.get_mask: END", 10)
        pass

    def get_mask_cidr(self, network):
        self._logger.debug("network.get_mask_cidr: BEGIN", 10)
        self._logger.debug("network.get_mask_cidr: END", 10)
        pass

    def get_network_address(self, network):
        self._logger.debug("network.get_network_address: BEGIN", 10)
        self._logger.debug("network.get_network_address: END", 10)
        pass

    def get_port(self, port, protocol="tcp"):
        # TODO: This function goes to Network class
        self._logger.debug("network.get_port: BEGIN", 10)
        self._logger.debug("network.get_port: Processing protocol{f1} \
port{f2}".format(f1=repr(protocol), f2=repr(port)), 90)
        # ICMP messages
        icmpmessages = {"echo-reply": 0, "unreachable": 3, "source-quench": 4,
                        "redirect": 5, "alternate-address": 6,
                        "echo": 8, "echo-request": 8,
                        "router-advertisement": 9, "router-solicitation": 10,
                        "time-exceeded": 11, "parameter-problem": 12,
                        "timestamp-request": 13, "timestamp-reply": 14,
                        "information-request": 15, "information-reply": 16,
                        "mask-request": 17, "mask-reply": 18, "traceroute": 30,
                        "conversion-error": 31, "mobile-redirect": 32
                        }

        initial_port = port
        fields = port.split(":")
        index = 0
        portoperand = None
        self._logger.debug("network.get_port: Fields loop.", 90)
        for field in fields:
            self._logger.debug(
                    "network.get_port: Processing fields {f1}".format(
                        f1=repr(field)), 90)
            if str(field).lower() in [
                    "eq", "ne", "lt", "gt", "=", "!=", "<>", "<", ">"]:
                portoperand = field
                self._logger.debug(
                        "network.get_port: Field is a portoperand.", 90)
                del(fields[index])
            else:
                try:
                    socket.getprotobyname(field)
                    protocol = field
                    self._logger.debug(
                            "network.get_port: Field is a protocol.", 90)
                    del(field[index])
                except Exception:
                    pass
            index += 1

        icmp_type = None
        if len(fields) == 2 and protocol.lower() == "icmp":
            icmp_type, port = fields
            self._logger.debug("network.get_port: Port is icmp.", 90)
        elif len(fields) == 2:
            protocol, port = fields
            self._logger.debug(
                "network.get_port: Port is protocol and port.", 90)
        elif len(fields) == 1 and port != "":
            port = fields[0]
            self._logger.debug("network.get_port: Port is a simple port.", 90)

        error = None
        if str(protocol).lower() in ["", "icmp"]:
            if str(port).lower() in ["any", ""]:
                port = str(icmpmessages["echo-request"]) + str(":0")
            elif icmp_type is not None:
                port = str(icmp_type) + ":" + str(int(port))
        elif str(portoperand).lower() in ["eq", "="]:
            try:
                port = int(port)
            except Exception:
                error = True
        elif str(portoperand).lower() in ["gt", ">", ">="]:
            try:
                port = int(port)+random.randint(int(port)+1, 65535)
            except Exception:
                error = True
        elif str(portoperand).lower() in ["lt", "<", "<="]:
            try:
                port = int(port) - random.randint(0, int(port) - 1)
            except Exception:
                error = True
        elif str(portoperand).lower() in ["neq", "!=", "<>"]:
            try:
                if random.randint(0, 100) < 50:
                    port = random.randint(int(port)+1, 65535)
                else:
                    port = random.randint(0, int(port) - 1)
            except Exception:
                error = True
        elif str(port).find("-") > 0:
            # Port is a range, so split it
            lower, upper = port.split("-")
            try:
                port = random.randint(int(lower), int(upper))
            except Exception:
                error = True
        elif str(port).lower() in ["any", "ANY", ""]:
            port = random.randint(0, 65535)
            self._logger.debug(
                "network.get_port: Port is any, random port is {f1}"
                .format(f1=repr(port))
            )
        elif port in icmpmessages:
            # swaps message name for message code
            port = int(icmpmessages[port])
        elif protocol.lower() == "esp":
            # TODO
            error = True
        else:
            try:
                # Checks If port is an integer
                int(port) + 1
            except Exception:
                error = True

        if error is not None:
            self._logger.error(
                "network.get_port: Invalid port or protocol {f1}, initial \
value is {f2}".format(f1=repr(port), f2=repr(initial_port))
            )
            raise ValueError(
                "network.get_port", "ERROR", "Invalid port or protocol {f1}, \
initial value is {f2}".format(f1=repr(port), f2=repr(initial_port))
            )
        self._logger.debug("""network.get_port:
            Method Variables:
                port: {f1}
                protocol: {f2}
                portoperand: {f3}
                icmp_type: {f4}
                error: {f5}""".format(
                f1=repr(port),
                f2=repr(protocol),
                f3=repr(portoperand),
                f4=repr(icmp_type),
                f5=repr(error)),
            80
        )
        self._logger.debug("network.get_port: END", 10)
        return [protocol, port]

    def is_host(self, address):
        self._logger.debug("network.is_host: BEGIN", 10)
        self._logger.debug("network.is_host: END", 10)
        pass

    def is_in_network(self, ip, network):
        self._logger.debug("network.is_in_network: BEGIN", 10)
        self._logger.debug("network.is_in_network: END", 10)
        pass

    def is_network(self, address):
        self._logger.debug("network.is_network: BEGIN", 10)
        self._logger.debug("network.is_network: END", 10)
        pass

    def is_subnet(self, network1, network2):
        self._logger.debug("network.is_subnet: BEGIN", 10)
        self._logger.debug("network.is_subnet: END", 10)
        pass
