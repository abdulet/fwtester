# -*- coding: utf-8 -*-
from abc import ABC, abstractmethod


class datastorageinterface():
    """Interface class for subclasses which manage storage of data"""

    @abstractmethod
    def add_row(self, data=[]):
        """Adds a row of data to the data storage"""
        pass

    def add_rows(self, data=[]):
        """Adds some rows of data to the data storage"""
        for row in data:
            self.add_row(row)

    @abstractmethod
    def get_row(self, number=0):
        """Gets a row of data from the data storage"""
        pass

    @abstractmethod
    def get_rows(self, num_of_rows=None, offset=0):
        """Gets some rows of data from the data storage"""
        pass
