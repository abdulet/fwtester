# -*- coding: utf-8 -*-


class argument():
    def __init__(self, name, default_value,
                 help_string, logger, required=False):
        self._logger = logger
        self._logger.debug("argument.__init__: BEGIN", 10)
        self.name = name
        self.default_value = default_value
        self.help_string = help_string
        self.required = required
        self._logger.debug("""argument.__init__:
            Method Variables:
                name: {f1}
                default_value: {f2}
                help_string: {f3}
                required: {f4}""".format(
                f1=repr(name),
                f2=repr(default_value),
                f3=repr(help_string),
                f4=repr(required)), 80
        )
        self._logger.debug("argument.__init__: END", 10)
