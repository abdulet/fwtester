# -*- coding: utf-8 -*-
import logging


class logger():
    "Class responsible of logging features for the code"

    def __init__(self, log_path, file_level=logging.DEBUG,
                 console_level=logging.ERROR,
                 log_format="%(asctime)s:%(name)s:%(levelname)s:%(message)s",
                 debug_level=0
                 ):
        self.debug_level = debug_level
        self.log_path = log_path
        self.file_level = file_level
        self.console_level = console_level
        self.log_format = log_format
        self._add_logger()

    def _add_logger(self):
        # Logging configuration
        logger = logging.getLogger(__name__)
        logger.setLevel(self.file_level)

        # Formater for logs
        formatter = logging.Formatter(self.log_format)

        # Log console handler
        ch = logging.StreamHandler()
        ch.setLevel(self.console_level)
        ch.setFormatter(formatter)

        # log file handler
        fh = logging.FileHandler(self.log_path, mode="w", encoding="utf-8")
        fh.setLevel(self.file_level)
        ch.setFormatter(formatter)

        logger.addHandler(ch)
        logger.addHandler(fh)
        self._logger = logger

    def debug(self, message, message_level=0):
        if message_level <= self.debug_level:
            self._logger.debug(message)

    def error(self, message):
        self._logger.error(message)

    def info(self, message):
        self._logger.info(message)

    def warning(self, message):
        self._logger.warning(message)
