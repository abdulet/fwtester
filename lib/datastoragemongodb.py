# -*- coding: utf-8 -*-
from lib.datastorageinterface import datastorageinterface
from pymongo import MongoClient


class datastoragemongodb(datastorageinterface):
    """Class to manage data storage of type MongoDB"""

    def __init__(self, database, logger):
        self._logger = logger
        self._logger.debug("datastoragemongodb.__init__: BEGIN", 10)
        self._client = MongoClient(port=27017)
        self._db = self._client[str(database)]
        self._logger.debug("""datastoragemongodb.__init__:
            Method Variables:
                database: {f1}
                logger: {f2}
                self._client: {f3}
                self._db: {f4}""".format(
                f1=repr(database),
                f2=repr(logger),
                f3=repr(self._client),
                f4=repr(self._db)), 80
        )
        self._logger.debug("datastoragemongodb.__init__: END", 10)

    def add_row(self, row, table):
        self._logger.debug("datastoragemongodb.add_row: BEGIN", 10)
        result = self._db[table].insert_one(row)
        self._logger.debug("""datastoragemongodb.add_row:
            Method Variables:
                row: {f1}
                table: {f2}
                result: {f3}""".format(
                f1=repr(row),
                f2=repr(table),
                f3=repr(result)), 80
        )
        self._logger.debug("datastoragemongodb.add_row: END", 10)

    def add_rows(self, rows, table):
        self._logger.debug("datastoragemongodb.add_rows: BEGIN", 10)
        result = self._db[str(table)].insert_many(rows)
        self._logger.debug("""datastoragemongodb.add_rows:
            Method Variables:
                rows: {f1}
                table: {f2}
                result: {f3}""".format(
                f1=repr(rows),
                f2=repr(table),
                f3=repr(result)), 80
        )
        self._logger.debug("datastoragemongodb.add_rows: END", 10)

    def get_rows(self, table, num_of_rows=None, offset=0):
        """
        @get_rows: read documents from a collection in MongoDB database
        """
        self._logger.debug("datastoragemongodb.get_rows: BEGIN", 10)
        lines = []
        iteration = 1
        for row in self._db[str(table)].find({}):
            iteration += 1
            if (num_of_rows is None or len(lines) <= num_of_rows) and\
                    (offset == 0 or iteration >= offset):
                lines.append(row)
        self._logger.debug("datastoragemongodb.get_rows: Rows retrieved begin",
                           100)
        self._logger.debug(repr(lines), 100)
        self._logger.debug("datastoragemongodb.get_rows: Rows retrieved end",
                           100)
        self._logger.debug("""datastoragemongodb.get_rows:
            Method Variables:
                table: {f1}
                num_of_rows: {f2}
                offset: {f3}""".format(
                f1=repr(table),
                f2=repr(num_of_rows),
                f3=repr(offset)), 80
        )
        self._logger.debug("datastoragemongodb.get_rows: END", 10)
        return lines
