#!/usr/bin/env /usr/bin/python3
# ############################################################
#                                                           #
#   This file is loaded into de server container,           #
#   open the required service and listen for a connection   #
#   from the server container                               #
#                                                           #
# ############################################################
import sys
import argparse
import traceback
import logging
import os
import importlib
import socket
from glob import glob
from time import strftime


# Exit code errors
OK = 0
ERROR = 1
ERR_TIMEOUT = 2
ERR_EXCEPTION = 255


class Server():
    def __init__(self, debug):
        # Logging configuration
        self._logger = logging.getLogger(__name__)
        self._logger.setLevel(logging.DEBUG)

        # #Formater for logs
        formatter = logging.Formatter(
            "%(asctime)s:%(name)s:%(levelname)s:%(message)s")

        # #Log console handler
        ch = logging.StreamHandler()
        ch.setLevel(logging.ERROR)
        ch.setFormatter(formatter)

        # #log file handler
        fh = logging.FileHandler("server.log", mode="w", encoding="utf-8")
        fh.setLevel(logging.DEBUG)
        ch.setFormatter(formatter)

        self._logger.addHandler(ch)
        self._logger.addHandler(fh)
        self._debug = debug

        # Protocol numbers
        self._PROTOCOLS = {
            "ICMP": 1,  # ICMP protocol type
            "IGMP": 2,  # Internet Group Management
            "TCP": 6,  # Transmission Control
            "EGP": 8,  # Exterior Gateway Protocol
            "IGP": 9,  # any private interior gateway
            # (used by Cisco for their IGRP)"
            "UDP": 17,  # User Datagram
            "DCCP": 33,  # Datagram Congestion Control Protocol
            "GRE": 47,  # Generic Routing Encapsulation
            "ESP": 50,  # Encap Security Payload
            "AH": 51,  # Authentication Header
            "EIGRP": 88  # EIGRP
        }
        self._modules = {"layers": {}}
        # self._modules["layers"][2] = {"protocol": {} #TODO
        self._modules["layers"][3] = {"protocols": {}}
        self._modules["layers"][7] = {"applications": {}}
        self._supported_protocols = []
        self._supported_applications = []

    def _add_module(self, package, name):
        self._logger.debug("Server._add_module: BEGIN")
        self._logger.debug(
            "Server._add_module: package {f1}, name {f2}".format(
                f1=repr(package), f2=repr(name)))
        module = importlib.import_module("."+name, package)
        cls = getattr(module, name)
        obj = cls(self._logger)
        if obj.LAYER in [2, 3, 7] and obj.PRIORITY >= 0:
            if obj.LAYER == 3 and obj.PROTOCOL_NUMBER not in\
                    self._modules["layers"][obj.LAYER]["protocols"]:
                self._modules["layers"][obj.LAYER]["protocols"][
                    obj.PROTOCOL_NUMBER] = {}
                self._modules["layers"][obj.LAYER]["protocols"][
                    obj.PROTOCOL_NUMBER][obj.PRIORITY] = obj
                self._supported_protocols.append(obj.PROTOCOL_NUMBER)
            if obj.LAYER == 7 and obj.APPLICATION.lower() not in\
                    self._modules["layers"][obj.LAYER]["applications"]:
                self._modules["layers"][obj.LAYER]["applications"][
                    obj.APPLICATION.lower()] = {}
                self._modules["layers"][obj.LAYER]["applications"][
                    obj.APPLICATION.lower()][obj.PRIORITY] = obj
                self._supported_applications.append(obj.APPLICATION.lower())
        else:
            self._logger.error(
                "Error loading module {!s}, bad property value in {!r}".
                format(name, obj)
            )
        self._logger.debug("Server._add_module: END")

    def load_modules(self):
        self._logger.debug("Server.load_modules: BEGIN")
        for path in [x[0] for x in os.walk("serverplugins")]:
            if path.find("__") > -1 or path == "serverplugins":
                # skips internal python files
                continue
            package = path.replace("/", ".")
            module = package.split(",")[-1]
            path = path + "/" + module + ".py"
            if os.path.isfile(path):
                self._add_module(package, module)
            else:
                self._logger.error(
                    "Error loading module {!s}, file doesn't exists".
                    format(module))
        # for path in glob("./serverplugins/*.py")[0:-1]:
        for path in glob("./serverplugins/*.py"):
            if path.find("__") > -1:
                # skips internal python files
                continue
            if os.path.isfile(path):
                package = path.split("/")[1]
                module = path.split("/")[2].replace(".py", "")
                self._add_module(package, module)
            else:
                self._logger.error(
                    "Error loading module {!s}, file doesn't exists".
                    format(module))
        self._logger.debug("Server.load_modules: END")

    def run_test(self, protocol, srcip, ip, srcport, dstport, searchfor,
                 layer=3, application=None):
        """
        Executes the test from the proper protocolo module with highest
        priority
        """
        self._logger.debug("Server.run_test: BEGIN")
        self._logger.debug(f"""Server.run_test:
    Received parameters:
        protocol: {protocol}
        srcip: {srcip}
        ip: {ip}
        srcport: {srcport}
        dstport: {dstport}
        searchfor: {searchfor}
        layer: {layer}
        application: {application}
        """)
        result = OK
        try:
            protocol_number = socket.getprotobyname(str(protocol).lower())
        except Exception:
            protocol_number = protocol
        if layer == 3 and protocol_number in self._supported_protocols:
            if layer in self._modules["layers"]:
                priority = list(
                    self._modules["layers"][layer]["protocols"]
                    [protocol_number].keys()
                )[-1]
                self._logger.debug(
                    "Server.run_test: Executing module for protocol {!s} with \
priority {!s}".format(protocol, priority))
                moduletorun = self._modules["layers"][layer]["protocols"][
                    protocol_number][priority]
            else:
                self._logger.error("ERROR in Server.run_test: Layer \
name:{!s} not implemented yet".format(layer))
                raise ValueError("ERROR in Server.run_test: Layer \
name:{!s} not implemented yet".format(layer))
        elif layer == 7 and application.lower() in \
                self._supported_applications:
            if layer in self._modules["layers"]:
                priority = list(
                    self._modules["layers"][layer]["applications"]
                    [application.lower()].keys()
                )[-1]
                self._logger.debug(
                    f"Server.run_test: Executing module for application \
{application.lower()} with priority {priority}")
                moduletorun = self._modules["layers"][layer][
                    "applications"][application.lower()][priority]
            else:
                self._logger.error("ERROR in Server.run_test: Application \
name:{!s} not implemented yet".format(application.lower()))
                raise ValueError("ERROR in Server.run_test: Application \
name:{!s} not implemented yet".format(application.lower()))
        else:
            if application != "":
                self._logger.error("ERROR in Server.run_test: Application \
name:{!s} not implemented yet".format(application.lower()))
                raise ValueError("ERROR in Server.run_test: Application \
name:{!s} not implemented yet".format(application.lower()))
            else:
                self._logger.error("ERROR in Server.run_test: Protocol name: \
{!s} and number: {!s} or application {!s} not implemented yet".
                               format(protocol, protocol_number, application))
                raise ValueError("ERROR in Server.run_test: Protocol name: \
{!s} and number: {!s} or application {!s} not implemented yet".
                             format(protocol, protocol_number, application))
        result = moduletorun.run(
            protocol_number, srcip, ip, srcport, dstport, searchfor)
        self._logger.debug("Server.run_test: END")
        return result


try:
    parser = argparse.ArgumentParser()
    parser.add_argument("-proto", "--protocol", dest="proto",
                        help="The protocol to use for connection", type=str)
    parser.add_argument("-dstip", "--destip", dest="dst",
                        help="The ip address of the server", type=str)
    parser.add_argument("-srcip", "--sourceip", dest="src",
                        help="The ip address of the server", type=str)
    parser.add_argument("-sp", "--sport", dest="sport",
                        help="The source port number to use for connection",
                        type=str)
    parser.add_argument("-dp", "--dport", dest="service", default="",
                        help="The port number to use for connection", type=str)
    parser.add_argument("-app", "--application", dest="app",
                        help="Name of the application to test, for L7 tests",
                        default=None, type=str)
    parser.add_argument("-search", "--searchfor", dest="searchfor", default="",
                        help="The string that server will send to check the\
 connection success", type=str)
    parser.add_argument("-dbg", "--debug", dest="debug", default=0,
                        help="Debug level [0 - 10]", type=str)
    args = vars(parser.parse_args())
    debug = args["debug"]

    server = Server(debug)
    server._logger.debug("Server: BEGIN")
    server.load_modules()
    # Emulates client and server with simple methods
    if args["app"] not in [None, ""]:
        ret = server.run_test(args["proto"], args["src"], args["dst"],
                              args["sport"], args["service"],
                              args["searchfor"], 7, args["app"])
    else:
        ret = server.run_test(args["proto"], args["src"], args["dst"],
                              args["sport"], args["service"],
                              args["searchfor"])
    if ret is not None and int(ret) == 0:
        # Connection succeed
        server._logger.debug("Server: Connection succeed")
        print("OK")
    else:
        # Connection failed
        server._logger.debug("Server: Connection failed")
        print("FAIL")
    server._logger.debug("Server: END")
except Exception:
    trace = str(traceback.format_exc())
    f = open("exception-server-"+strftime("%Y_%m_%d-%H_%M"), "w")
    f.write(trace)
    f.close()
    try:
        server._logger.debug(trace)
    except Exception:
        print(trace)
    sys.exit(ERR_EXCEPTION)
sys.exit(OK)
