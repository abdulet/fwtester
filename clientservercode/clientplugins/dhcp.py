"""This module sends a dhcp request packet.

It's the client side plugin for dhcp tester,
it can be run with server side plugin or
to a real dhcp server, and waits for a dhcp offer.
"""
import ipaddress
import logging
import sys
if "unittest" in sys.modules:
    import os
    import unittest

from scapy.all import Ether, UDP, IP, RandMAC, BOOTP, DHCP, srp1


class dhcp:
    """This is the dhcp plugin client side class.

    This plugin is intended to run at layer 7 for dhcp protocol.
    It's composed by 2 methods:
    __init__: The standard init method which receives a logger object.
    run: The method which will be called by Client.py to run the
         plugin code.

    This plugin use the following constants
        LAYER = 7 (Applications layer)
        PRIORITY = 1 (first plugin for DHCP)
        APPLICATION = "dhcp" (Application name for the plugin)
    And these exit codes:
        OK = 0
        ERROR = 1
        ERR_TIMEOUT = 2
        ERR_EXCEPTION = 255
    """
    # Exit codes
    OK = 0
    ERROR = 1
    ERR_TIMEOUT = 2
    ERR_EXCEPTION = 255

    # Module level constants
    LAYER = 7
    PRIORITY = 1
    APPLICATION = "dhcp"

    def __init__(self, logger):
        """This method inizializes the logger and local variables.

        Args:
            logger (logging.logger): Logger object to use
            to write log messages. Defaults to None.
        """
        self._logger = logger
        self._proto = self._ip = self._srcport = self._dstport = None

    def run(self, proto=17, srcip="0.0.0.0", address="255.255.255.255",
            srcport=68, dstport=67, searchfor=None):
        """This function generate an standard dhcp discover packet.

        Sends dhcp discover and waits for a response or timesout.
        This method ensures that the received parameters are RFC
        compliance, if aren't, sets them to the RFC values. So you can't
        use this plugin to test non standard DHCP scenarios.

        Args:
            proto (int, optional): The layer 3 protcolo to use.
                Must be 17 (UDP).
            address (str, optional): Server IP address.
                Defaults to multicast address "255.255.255.255".
            srcport (int, optional): Layer 3 source port.
                Defaults and must be 68.
            dstport (int, optional): Layer 3 destination port.
                Defaults and must be 67.
            searchfor (str, optional): Not used by the module.
                Defaults to None.
        """
        return_state = self.OK
        try:
            # Logs begin of function
            self._logger.debug("dhcp.run: BEGIN")

            # Store ip in a private object property
            self._ip = address
            try:
                address = ipaddress.ip_address(address)
            except ValueError:
                self._logger.warning(
                    "dhcp.run: IP address is not valid set it to \
multicast 255.255.255.255")
                self._ip = "255.255.255.255"

            # Checks if protocol is udp, if not sets it to UDP
            if proto != int(17):
                self._logger.warning(
                    "dhcp.run: protocol does not follows RFC set it to \
udp(17)")
                proto = 17

            # Store protocol in a private object property
            self._proto = int(proto)

            # Check if srcport is int(68)
            # if is not set it to 68 following the DHCP's RFC
            if srcport != int(68):
                self._logger.warning(
                    "dhcp.run: srcport does not follows RFC set it to 68"
                )
                srcport = 68
            # Store srcport in a private object property
            self._srcport = int(srcport)

            # Check if dstport is int(67)
            # if is not set it to 67 following the DHCP's RFC
            if dstport != int(67):
                self._logger.warning(
                    "dhcp.run: dstport does not follows RFC set it to 67"
                )
                dstport = 67
            # Store dstport in a private object property
            self._dstport = int(dstport)

            self._logger.debug(
                f"dhcp.run: Sending request protocol: {self._proto} to: \
{self._ip} srcport: {self._srcport} dstport: {self._dstport}")

            # Create a DHCP request
            dhcp_discover = Ether(dst="ff:ff:ff:ff:ff:ff")/IP(
                dst=str(self._ip))/UDP(
                    sport=int(self._srcport), dport=int(self._dstport))/BOOTP(
                        chaddr=RandMAC())/DHCP(
                            options=[("message-type", "discover"), "end"])

            # Send the request and wait for the response
            dhcp_offer = srp1(dhcp_discover, timeout=3, verbose=0)
            # Writes down the assigned IP address to log
            if dhcp_offer:
                self._logger.debug(f"dhcp.run: Assigned IP address: \
{dhcp_offer[BOOTP].yiaddr}")
            else:
                self._logger.warning(
                    "dhcp.run: Timeout No response received")
                return_state = self.ERR_TIMEOUT
        except PermissionError as error:
            self._logger.exception(
                f"dhcp.run: Operation not permitted: {error}")
            return_state = self.ERR_EXCEPTION
        except ValueError as error:
            self._logger.exception(
                f"dhcp.run: A ValueError occurred: {error}")
            return_state = self.ERR_EXCEPTION
        except OSError as error:
            self._logger.exception(
                f"dhcp.run: An OSError occurred: {error}")
            return_state = self.ERR_EXCEPTION
        except Exception as error:
            self._logger.exception(
                f"dhcp.run: An unhandled error occurred: {error}")
            return_state = self.ERR_EXCEPTION
        self._logger.debug("dhcp.run: END")
        return return_state


# Unittest code
if "unittest" in sys.modules:
    class Testdhcp(unittest.TestCase):
        """Unittest module class"""

        def test_run(self):
            """Test the run method of dhcp class"""
            # Create a logger for the test
            logger = logging.getLogger("output/logs/testunits.log")

            # Create a dhcp instance
            client = dhcp(logger)

            # Check for correct exit codes
            self.assertEqual(client.OK, int(0))
            self.assertEqual(client.ERROR, int(1))
            self.assertEqual(client.ERR_TIMEOUT, int(2))
            self.assertEqual(client.ERR_EXCEPTION, int(255))

            # Tests that logger is correctly setup by __init__
            self.assertTrue(isinstance(
                client._logger, logging.Logger), "Bad __init__ value")

            # Tests that LAYER is correctly setup by __init__
            self.assertEqual(client.LAYER, int(7), "Bad __init__ value")

            # Tests that LAYER is correctly setup by __init__
            self.assertEqual(client.PRIORITY, int(1), "Bad __init__ value")

            # Tests that LAYER is correctly setup by __init__
            self.assertEqual(client.APPLICATION, "dhcp", "Bad __init__ value")

            # Call the run method and check the return value
            # as this is a simple test ther shouldn't be a response
            # so a timeout is expected, and a good result for test
            # the test also set default values for params
            if os.geteuid() == 0:
                self.assertEqual(client.run(), client.ERR_TIMEOUT)
            else:
                print("This test should be run as root")
                sys.exit(1)

            # Checks that default values are properly set
            self.assertEqual(client._proto, int(17), "Bad default value")
            self.assertEqual(
                client._ip, "255.255.255.255", "Bad default value")
            self.assertEqual(client._srcport, int(68), "Bad default value")
            self.assertEqual(client._dstport, int(67), "Bad default value")

            # No testing with bad params to check sanitization
            self.assertEqual(client.run(
                proto="proto", address="ip", srcport="srcport",
                dstport="dstport"), client.ERR_TIMEOUT)

            # Checks that default values are properly set
            self.assertEqual(client._proto, int(17), "Fuzzed value accepted")
            self.assertEqual(
                client._ip, "255.255.255.255", "Fuzzed value accepted")
            self.assertEqual(client._srcport, int(68), "Fuzzed value accepted")
            self.assertEqual(client._dstport, int(67), "Fuzzed value accepted")
