import socket


class tcp():
    # Exit codes
    OK = 0
    ERROR = 1
    ERR_TIMEOUT = 2
    ERR_EXCEPTION = 255

    LAYER = 3
    PROTOCOL_NUMBER = 6
    PRIORITY = 1

    def __init__(self, logger):
        self._logger = logger

    def get_service(self, service, numericformat=True):
        if numericformat:
            if service.lower() in ["dns"]:
                service = "domain"
            try:
                return socket.getservbyname(service.lower())
            except (ValueError, OSError):
                pass
        else:
            try:
                return socket.getservbyport(int(service))
            except (ValueError, OSError):
                pass
        return service

    def run(self, proto, srcip, ip, srcport, dstport, searchfor):
        """
        Send a tcp data and wait for response
        """

        self._logger.debug("tcp.run: BEGIN")

        try:
            socket.allow_reuse_address = True
            self._logger.debug("tcp.run: Building tcp socket")
            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM, int(proto))
            self._logger.debug(
                "tcp.run: Binding to src port {f1}".format(f1=repr(srcport)))
            s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            s.bind(("", int(srcport)))

            # Gets the service number
            service = self.get_service(dstport)
            self._logger.debug(
                "tcp.run: Service number is {f1}".format(f1=repr(service)))
        except socket.error as msg:
            self._logger.debug(
                "tcp.run: Socket generation failed. Error Code: {f1} msg {f2}".
                format(f1=repr(msg[0]), f2=repr(msg[1])))
            self._logger.debug("tcp.run: END")
            return self.ERROR
        try:
            sockopen = False
            try:
                s.connect((str(ip), int(service)))
                self._logger.debug("tcp.run: Opening tcp socket")
                sockopen = True
            except ConnectionRefusedError:
                self._logger.debug("tcp.run: Connection to server failed. \
Error: ConnectionRefusedError")
                return self.ERROR
            except Exception as e:
                self._logger.debug(
                    "tcp.run: Connection to server failed.Error: {f1} msg {f2}".
                    format(f1=repr(e[0]), f2=repr(e[1])))
                return self.ERROR

            if str(searchfor) != "":
                # Sending message to server
                s.send((searchfor+"\n").encode("utf-8"))
                self._logger.debug(
                    "tcp.run: Message sended by tcp {f1}".format(
                        f1=repr(searchfor)))

                recived = ""
                while True:
                    # Receiving from server
                    data = s.recv(1024).decode("utf-8")
                    recived = recived+data
                    self._logger.debug(
                        "tcp.run: Secket data recived {f1}".format(
                            f1=repr(recived)))
                    if str(recived).replace("\n", "") == str(searchfor):
                        self._logger.debug(
                            "tcp.run: Search for match found {f1}".format(
                                f1=repr(recived))
                        )
                        break
        finally:
            if sockopen:
                # Close client socket
                s.shutdown(1)
                s.close()
                self._logger.debug("tcp.run: Socket closed")
        self._logger.debug("tcp.run: END")
        return self.OK
