"""This module sends a dns request packet.

It's the client side plugin for dns tester,
it can be run with server side plugin or
to a real dns server, and waits for a dns offer.
"""
import ipaddress
import logging
import socket
import sys
if "unittest" in sys.modules:
    import os
    import unittest

from scapy.all import IP, TCP, UDP, sr1, DNS, DNSQR


class dns:
    """This is the dns plugin client side class.

    This plugin is intended to run at layer 7 for dns protocol.
    It's composed by 2 methods:
    __init__: The standard init method which receives a logger object.
    run: The method which will be called by Client.py to run the
         plugin code.

    This plugin use the following constants
        LAYER = 7 (Applications layer)
        PRIORITY = 1 (first plugin for dns)
        APPLICATION = "dns" (Application name for the plugin)
    And these exit codes:
        OK = 0
        ERROR = 1
        ERR_TIMEOUT = 2
        ERR_EXCEPTION = 255
    """
    # Exit codes
    OK = 0
    ERROR = 1
    ERR_TIMEOUT = 2
    ERR_EXCEPTION = 255

    # Module level constants
    LAYER = 7
    PRIORITY = 1
    APPLICATION = "dns"

    def __init__(self, logger):
        """This method inizializes the logger and local variables.

        Args:
            logger (logging): Logger object to use
            to write log messages. Defaults to None.
        """
        self._logger = logger
        self._proto = self._ip = self._srcport = self._dstport = None

    def run(self, proto, srcip, address, srcport, dstport, searchfor=None,
            timeout=0):
        """This function generate an standard dns discover packet.

        Sends dns discover and waits for a response or timesout.

        Args:
            proto (int): The layer 3 protcolo to use.
                Can be 6 (TCP) or 17 (UDP).
            address (str): Server IP address.
            srcport (int): Layer 3 source port.
            dstport (int): Layer 3 destination port.
            searchfor (str, optional): Not used by the module.
                Defaults to None.
        """
        return_state = self.OK
        try:
            # Logs begin of function
            self._logger.debug("dns.run: BEGIN")

            # Store source ip in a private object property
            self._srcip = srcip
            try:
                ipaddress.ip_address(srcip)
            except ValueError:
                self._logger.warning(
                    f"dns.run: SRC IP address is not valid {srcip}")
                return self.ERR_EXCEPTION

            # Store destination ip in a private object property
            self._ip = address
            try:
                ipaddress.ip_address(address)
            except ValueError:
                self._logger.warning(
                    f"dns.run: DST IP address is not valid {address}")
                return self.ERR_EXCEPTION

            # Checks if protocol is valid
            if proto != int(17) and proto != int(6):
                self._logger.error(f"dns.run: Invalid protocol {proto}")
                return self.ERR_EXCEPTION
            # Store protocol in a private object property
            self._proto = int(proto)

            # Check if srcport is valid
            try:
                # Store srcport in a private object property
                self._srcport = int(srcport)
            except ValueError:
                self._logger.error(
                    f"dns.run: Invalid source port {srcport}"
                )
                return self.ERR_EXCEPTION

            # Check if dstport is valid
            try:
                # Store dstport in a private object property
                self._dstport = int(dstport)
            except ValueError:
                self._logger.error(
                    f"dns.run: Invalid destination port {dstport}"
                )
                return self.ERR_EXCEPTION

            self._logger.debug(
                f"dns.run: Sending request protocol: {self._proto} to: \
{self._ip} srcport: {self._srcport} dstport: {self._dstport}")

            # Geneartes the socket (tcp or udp)
            if self._proto == 17:
                # Generates an UDP socket
                self._sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
                self._sock.bind((self._srcip, self._srcport))
                transport = UDP(
                    sport=int(self._srcport), dport=int(self._dstport))
            else:
                # Generates a TCP socket
                self._sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                self._sock.settimeout(timeout)
                self._sock.bind((self._srcip, self._srcport))
                transport = TCP(
                    sport=int(self._srcport), dport=int(self._dstport))
            dns_req = IP(dst=self._ip)/transport/DNS(
                rd=1, qd=DNSQR(qname='iana.org'))
            sr1(dns_req, verbose=0, timeout=timeout)

            # Close the socket
            self._sock.close()
        except socket.timeout:
            self._logger.exception("ntp.run: Connection timed out")
            return_state = self.ERR_TIMEOUT
            self._sock.close()
        except PermissionError as error:
            self._logger.exception(
                f"dns.run: Operation not permitted: {error}")
            return_state = self.ERR_EXCEPTION
        except ValueError as error:
            self._logger.exception(
                f"dns.run: A ValueError occurred: {error}")
            return_state = self.ERR_EXCEPTION
        except OSError as error:
            self._logger.exception(
                f"dns.run: An OSError occurred: {error}")
            return_state = self.ERR_EXCEPTION
        except Exception:
            self._logger.exception(
                "dns.run: An unhandled error occurred: {error}")
            return_state = self.ERR_EXCEPTION
        self._logger.debug("dns.run: END")
        return return_state


# Unittest code
if "unittest" in sys.modules:
    class Testdns(unittest.TestCase):
        """Unittest module class"""

        def test_run(self):
            """Test the run method of dns class"""
            # Create a logger for the test
            logger = logging.getLogger("output/logs/testunits.log")

            # Create a dns instance
            client = dns(logger)

            # Check for correct exit codes
            self.assertEqual(client.OK, int(0))
            self.assertEqual(client.ERROR, int(1))
            self.assertEqual(client.ERR_TIMEOUT, int(2))
            self.assertEqual(client.ERR_EXCEPTION, int(255))

            # Tests that logger is correctly setup by __init__
            self.assertTrue(isinstance(
                client._logger, logging.Logger), "Bad __init__ value")

            # Tests that LAYER is correctly setup by __init__
            self.assertEqual(client.LAYER, int(7), "Bad __init__ value")

            # Tests that PRIORITY is correctly setup by __init__
            self.assertEqual(client.PRIORITY, int(1), "Bad __init__ value")

            # Tests that APPLICATION is correctly setup by __init__
            self.assertEqual(client.APPLICATION, "dns", "Bad __init__ value")

            # Call the run method for UDP and check the return value
            # as this is a simple test there shouldn't be a response
            # so a timeout is expected, as a good result for test
            dport = int(553)
            if os.geteuid() == 0:
                self.assertEqual(client.run(
                    17, "0.0.0.0", "10.0.0.1", 2346, dport, timeout=1),
                    client.OK)
            else:
                print("This test should be run as root")
                sys.exit(1)

            # Checks that values are properly set
            self.assertEqual(client._proto, int(17), "Bad IP protocol")

            self.assertTrue(
                isinstance(ipaddress.ip_address(client._ip),
                           ipaddress.IPv4Address), "Bad IP address")
            self.assertTrue(isinstance(client._srcport, int),
                            "Bad source port")
            self.assertTrue(isinstance(client._dstport, int),
                            "Bad destination port")

            # Call the run method for TCP and check the return value
            # as this is a simple test there shouldn't be a response
            # so a timeout is expected, as a good result for test
            if os.geteuid() == 0:
                self.assertEqual(client.run(
                    6, "0.0.0.0", "10.0.0.1", 2348, dport, timeout=1),
                    client.ERR_TIMEOUT)
            else:
                print("This test should be run as root")
                sys.exit(1)

            # Checks that values are properly set
            self.assertEqual(client._proto, int(6), "Bad IP protocol")

            self.assertTrue(
                isinstance(ipaddress.ip_address(client._ip),
                           ipaddress.IPv4Address), "Bad IP address")
            self.assertTrue(isinstance(client._srcport, int),
                            "Bad source port")
            self.assertTrue(isinstance(client._dstport, int),
                            "Bad destination port")

            # Now testing with bad protocol
            self.assertEqual(client.run(
                proto="proto", srcip="10.10.10.2", address="192.168.0.1",
                srcport=4567, dstport=dport), client.ERR_EXCEPTION,
                "Bad protocol accepted")

            # Now testing with bad source IP
            self.assertEqual(client.run(
                proto=6, srcip="ipaddr", address="192.168.0.1",
                srcport=4567, dstport=dport), client.ERR_EXCEPTION,
                "Bad source IP accepted")

            # Now testing with bad destination IP
            self.assertEqual(client.run(
                proto=6, srcip="10.10.10.2", address="ipaddr",
                srcport=4567, dstport=dport), client.ERR_EXCEPTION,
                "Bad destination IP accepted")

            # Now testing with bad source port
            self.assertEqual(client.run(
                proto=6, srcip="10.10.10.2", address="192.168.0.1",
                srcport="port", dstport=dport), client.ERR_EXCEPTION,
                "Bad source port accepted")

            # Now testing with bad destination port
            self.assertEqual(client.run(
                proto=6, srcip="10.10.10.2", address="192.168.0.1",
                srcport=4567, dstport="port"), client.ERR_EXCEPTION,
                "Bad destination port accepted")
