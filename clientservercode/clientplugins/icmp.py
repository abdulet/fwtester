from scapy.all import sr1, IP, ICMP, TCP, UDP, packet


class icmp():
    # Exit codes
    OK = 0
    ERROR = 1
    ERR_TIMEOUT = 2
    ERR_EXCEPTION = 255

    LAYER = 3
    PROTOCOL_NUMBER = 1
    PRIORITY = 1

    def __init__(self, logger):
        self._logger = logger

    def run(self, proto, srcip, ip, srcport, dstport, searchfor):
        "Sends icmp packet and wait for 1 response"
        self._logger.debug("icmp.run: BEGIN")
        icmp_responses = {
            8: {"type": [0, 3, 11],
                "code": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]},  # echo request, returns; echo reply, time exeeded, destination unreachable
            # information request, return information replay
            15: {"type": [16], "code": [0]},
            # Address Mask Request, return address mask reply
            17: {"type": [18], "code": [0]},
            # information request (traceroute), return information replay (deprecated)
            30: {"type": 16, "code": 0}
        }
        try:
            icmp_type, icmp_code = dstport.split(":")
        except:
            # Set message to icmp echo-request
            icmp_type = 8
            icmp_code = 0
        self._logger.debug("icmp.run: icmp type {f1}, code {f2}".format(
            f1=repr(icmp_type), f2=repr(icmp_code)))
        if int(icmp_type) not in icmp_responses:
            self._logger.error(
                "ERROR in function send: ICMP type {f1} not yet implemented.".
                format(f1=repr(icmp_type))
            )
            raise ValueError(
                "ERROR in function send: ICMP type {f1} not yet implemented.".
                format(f1=repr(icmp_type))
            )

        ans = sr1(IP(dst=ip)/ICMP(type=int(icmp_type), code=int(icmp_code)),
                  timeout=3, retry=-1)
        result = self.ERR_TIMEOUT
        if ans is not None:
            anstype = ans.getlayer("ICMP").type
            anscode = ans.getlayer("ICMP").code
            self._logger.debug("icmp.run: answer icmp type {f1}, code {f2}".
                               format(f1=repr(anstype), f2=repr(anscode)))
            self._logger.debug("icmp.run: icmp_responses for icmp_type is {f1}".
                               format(f1=repr(icmp_responses[int(icmp_type)])))
            if int(anstype) in icmp_responses[int(icmp_type)]["type"] and \
                    int(anscode) in icmp_responses[int(icmp_type)]["code"]:
                result = self.OK
            else:
                self._logger.error("ERROR in function send: ICMP type {f1} or \
     {f2} unknown for request {f3}:{f4}".
                                   format(f1=rerp(anstype), f2=repr(anscode),
                                          f3=repr(icmp_type), f4=repr(icmp_code))
                                   )
                raise ValueError("ERROR in function send: ICMP type {f1} or \
     {f2} unknown for request {f3}:{f4}".
                                 format(f1=rerp(anstype), f2=repr(anscode),
                                        f3=repr(icmp_type), f4=repr(icmp_code))
                                 )

        self._logger.debug("icmp.run: END")
        return result
