import socket


class udp():
    # Exit codes
    OK = 0
    ERROR = 1
    ERR_TIMEOUT = 2
    ERR_EXCEPTION = 255

    LAYER = 3
    PROTOCOL_NUMBER = 17
    PRIORITY = 1

    def __init__(self, logger):
        self._logger = logger

    def _get_service(self, service, numericformat=True):
        if numericformat:
            if service.lower() in ["dns"]:
                service = "domain"
            try:
                return socket.getservbyname(service.lower())
            except (ValueError, OSError):
                pass
        else:
            try:
                return socket.getservbyport(int(service))
            except (ValueError, OSError):
                pass
        return service

    def run(self, proto, srcip, ip, srcport, service, searchfor):
        """
        Send a udp data and wait for response
        """

        self._logger.debug("udp.run: BEGIN")

        try:
            socket.allow_reuse_address = True
            self._logger.debug("udp.run: Building udp socket")
            s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, int(proto))
            self._logger.debug("udp.run: Binding to src port {f1}".format(
                f1=repr(srcport)))
            s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            s.bind(("", int(srcport)))

            # Gets the service number
            service = self._get_service(service)
            self._logger.debug("udp.run: Service number is {f1}".format(
                f1=repr(service)))
        except socket.error as msg:
            self._logger.debug(
                "udp.run: Socket generation failed. Error Code: {f1} age {f2}".
                format(f1=repr(msg[0]), f2=repr(msg[1])))
            self._logger.debug("udp.run: END")
            return self.ERROR
        try:
            sockopen = False

            if str(searchfor) != "":
                # Sending message to server
                s.sendto(
                    (searchfor+"\n").encode("utf-8"), (str(ip), int(service)))
                self._logger.debug(
                    "udp.run: Message sended by udp {f1}".format(
                        f1=repr(searchfor)))

                recived = ""
                while True:
                    # Receiving from server
                    data = s.recv(1024).decode("utf-8")
                    recived = recived+data
                    self._logger.debug(
                        "udp.run: Secket data recived {f1}".format(
                            f1=repr(recived)))
                    if str(recived).replace("\n", "") == str(searchfor):
                        self._logger.debug(
                            "udp.run: Search for match found {f1}".format(
                                f1=repr(recived)))
                        break
        finally:
            if sockopen:
                # Close client socket
                s.shutdown(1)
                s.close()
                self._logger.debug("udp.run: Socket closed")
        self._logger.debug("udp.run: END")
        return self.OK
