from scapy.all import sniff, send, IP, ICMP, TCP, UDP, packet


class icmp():
    # Exit codes
    OK = 0
    ERROR = 1
    ERR_TIMEOUT = 2
    ERR_EXCEPTION = 255

    LAYER = 3
    PROTOCOL_NUMBER = 1
    PRIORITY = 1

    def __init__(self, logger):
        self._logger = logger

    def _stop_sniff(self, packet):
        self._logger.debug("BEGIN: stop_sniff")
        icmp_responses = {
            8: {"type": 0, "code": 0},   # echo request, return echo reply
            # information request, return information replay (deprecated)
            15: {"type": 16, "code": 0},
            # Address Mask Request, return address mask reply
            17: {"type": 18, "code": 0},
            # information request (traceroute), return information replay (deprecated)
            30: {"type": 16, "code": 0},
        }

        # Sends ICMP response to client
        icmptype = packet[0].getlayer("ICMP").type
        icmpcode = packet[0].getlayer("ICMP").code
        print("Receive ICMP type: {f1}, ICMP code: {f2}".format(
            f1=repr(icmptype), f2=repr(icmpcode)))
        self._logger.debug(
            "stop_sniff: Receive ICMP type: {f1}, ICMP code: {f2}"
            .format(f1=repr(icmptype), f2=repr(icmpcode)))
        print("Send ICMP type: {f1}, ICMP code: {f2}".format(
            f1=repr(icmp_responses[icmptype]["type"]),
            f2=repr(icmp_responses[icmptype]["code"])))
        self._logger.debug("stop_sniff: Send ICMP type: {f1}, ICMP code: {f2}"
                           .format(f1=repr(icmp_responses[icmptype]["type"]),
                                   f2=repr(icmp_responses[icmptype]["code"])))
        send(IP(dst=args["src"])/ICMP(type=icmp_responses[icmptype]["type"],
                                      code=icmp_responses[icmptype]["code"]))
        self._logger.debug("END: stop_sniff")
        return self.OK

    def run(self, proto, srcip, ip, srcport, dstport, searchfor):
        "Recive icmp packet and sends 1 response"
        self._logger.debug("icmp.run: BEGIN")
        icmp_responses = {
            8: {"type": [0, 3, 11],
                "code": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]},  # echo request, returns; echo reply, time exeeded, destination unreachable
            # information request, return information replay
            15: {"type": [16], "code": [0]},
            # Address Mask Request, return address mask reply
            17: {"type": [18], "code": [0]},
            # information request (traceroute), return information replay (deprecated)
            30: {"type": 16, "code": 0}
        }
        try:
            icmp_type, icmp_code = dstport.split(":")
        except:
            # Set message to icmp echo-request
            icmp_type = 8
            icmp_code = 0
        if int(icmp_type) == 8:
            self._logger.info("Packets of type icmp echo_request are managed directly by de linux networking so exiting")
            exit(0)
        self._logger.debug("icmp.run: icmp type {f1}, code {f2}".format(
            f1=repr(icmp_type), f2=repr(icmp_code)))
        if int(icmp_type) not in icmp_responses:
            self._logger.error(
                "ERROR in function send: ICMP type {f1} not yet implemented.".
                format(f1=repr(icmp_type))
            )
            raise ValueError(
                "ERROR in function send: ICMP type {f1} not yet implemented.".
                format(f1=repr(icmp_type))
            )
        print('Server now listening')
        sniff(prn=self._stop_sniff,
              filter="icmp and src {f1}".format(f1=ip))
