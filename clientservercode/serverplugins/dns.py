"""This plugin waits for a dns discover packet
and returns a dns offer.
It's the server side plugin for dns tester server one.
"""

import ipaddress
import logging
import socket
import sys
if "unittest" in sys.modules:
    import os
    import unittest

from scapy.all import IP, TCP, UDP, DNS, DNSRR, send, Scapy_Exception
from scapy.all import sniff


class dns:
    """This is the dns plugin server side class.

    This plugin is intended to run at layer 7 for dns protocol.
    It's composed by 2 methods:
    __init__: The standard init method which receives a logger object.
    run: The method which will be called by server.py to run the
         plugin code.
    listen: Start the listen socket and waits for packets
    process_request: Each received packet is processed by this fucntion

    This plugin use the following constants
        LAYER = 7 (Applications layer)
        PRIORITY = 1 (first plugin for dns)
        APPLICATION = "dns" (Application name for the plugin)
    And these exit codes:
        OK = 0
        ERROR = 1
        ERR_TIMEOUT = 2
        ERR_EXCEPTION = 255
    """

    # Exit codes
    OK = 0
    ERROR = 1
    ERR_TIMEOUT = 2
    ERR_EXCEPTION = 255

    LAYER = 7
    PRIORITY = 1
    APPLICATION = "dns"

    def __init__(self, logger):
        """This method inizializes the logger and local variables.

        Args:
            logger (logging.logger): Logger object to use
            to write log messages. Defaults to None.
        """

        self._logger = logger
        self._proto = self._ip = self._srcport = self._dstport = None

    def run(self, proto, srcip, address, srcport, dstport, searchfor=None,
            timeout=None):
        """This function generate an standard dns response packet.

        Waits for a dns query packet and sends back a dns response and exit.

        Args:
            proto (int): The layer 3 protcolo to use.
                Can be 6 (TCP) or 17 (UDP).
            srcip (str): Client IP address.
            address (str): Server IP address.
            srcport (int): Layer 3 source port.
            dstport (int): Layer 3 destination port.
            searchfor (str, optional): Not used by the module.
                Defaults to None.
        """

        return_state = self.OK
        try:
            # Logs begin of function
            self._logger.debug("dns.run: BEGIN")

            self._timeout = timeout

            # Store ip in a private object property
            self._srcip = srcip
            try:
                ipaddress.ip_address(srcip)
            except ValueError:
                self._logger.warning(
                    f"dns.run: SRC IP address is not valid {srcip}")
                return self.ERR_EXCEPTION

            # Store ip in a private object property
            self._ip = address
            try:
                address = ipaddress.ip_address(address)
            except ValueError:
                self._logger.warning(
                    f"dns.run: DST IP address is not valid {address}")
                return self.ERR_EXCEPTION

            # Checks if protocol is valid
            if proto != int(17) and proto != int(6):
                self._logger.error(f"dns.run: Invalid protocol {proto}")
                return self.ERR_EXCEPTION
            # Store protocol in a private object property
            self._proto = int(proto)

            # Check if srcport is valid
            try:
                # Store srcport in a private object property
                self._srcport = int(srcport)
            except ValueError:
                self._logger.error(
                    f"dns.run: Invalid source port {srcport}"
                )
                return self.ERR_EXCEPTION

            # Check if dstport is valid
            try:
                # Store dstport in a private object property
                self._dstport = int(dstport)
            except ValueError:
                self._logger.error(
                    f"dns.run: Invalid destination port {dstport}"
                )
                return self.ERR_EXCEPTION

            # Liste for DNS requests
            self.listen()

            # Close the socket
            self._sock.close()
        except ValueError as error:
            self._logger.exception(
                f"dns.run: A ValueError occurred: {error}")
            return_state = self.ERR_EXCEPTION
        except OSError as error:
            self._logger.exception(
                f"dns.run: An OSError occurred: {error}")
            return_state = self.ERR_EXCEPTION
        except TypeError as error:
            self._logger.exception(
                f"dns.run: A TypeError occurred: {error}")
            return_state = self.ERR_EXCEPTION
        except Scapy_Exception as error:
            self._logger.exception(
                f"dns.run: An Scapy exception occurred: {error}")
            return_state = self.ERR_EXCEPTION
        except Exception as error:
            self._logger.exception(
                f"dns.run: An unhandled error occurred: {error}")
            return_state = self.ERR_EXCEPTION
        self._logger.debug("dns.run: END")
        return return_state

    def listen(self):
        self._logger.debug("dns.listen: BEGIN")
        # Geneartes the socket (tcp or udp)
        if self._proto == 17:
            # Generates an UDP socket
            self._sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            self._sock.bind((self._ip, self._dstport))
            self._logger.debug(
                f"dns.listen: Listening UDP at {self._ip}:{self._dstport}")
            filter = f"udp and port {self._dstport}"
        else:
            # Generates a TCP socket
            self._sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self._sock.bind((self._ip, self._dstport))
            self._sock.listen(1)
            self._logger.debug(
                f"dns.listen: Listening TCP {self._ip}:{self._dstport}")
            filter = f"tcp and port {self._dstport}"

        self._logger.debug(
            f"dns.listen: Sniffing request protocol: {self._proto} \
to: {self._ip} srcport: {self._srcport} dstport: {self._dstport}")

        # Start sniffing for dns requests
        sniff(prn=self.process_request, filter=filter, store=0, count=1,
              timeout=self._timeout)
        self._logger.debug("dns.listen: END")

    def process_request(self, packet):
        self._logger.debug("dns.process_request: BEGIN")
        # Check if the packet is a dns request
        return_state = self.OK
        if DNS in packet:
            if packet[IP].src != self._srcip:
                # Request is not from client side module
                return return_state

            # self._logger.debug the request information
            self._logger.debug(
                f"dns.process_request: dns request received from IP \
{self._srcip}")
            dns = DNS(
                id=packet[DNS].id,
                qd=packet[DNS].qd,
                aa=1,
                rd=0,
                qr=1,
                qdcount=1,
                ancount=1,
                nscount=0,
                arcount=0,
                ar=DNSRR(
                    rrname=packet[DNS].qd.qname,
                    type='A',
                    ttl=600,
                    rdata='1.2.3.4')
            )
            # Create a dns response
            if TCP in packet:
                self._logger.debug(
                    f"dns.process_request: Sending TCP response to \
{packet[IP].src} from {packet[IP].dst}")
                dns_response = IP(src=packet[IP].dst, dst=packet[IP].src)/TCP(
                    sport=packet[TCP].dport, dport=packet[TCP].sport)/dns
            else:
                self._logger.debug(
                    f"dns.process_request: Sending UDP response to \
{packet[IP].src} from {packet[IP].dst}")
                dns_response = IP(src=packet[IP].dst, dst=packet[IP].src)/UDP(
                    sport=packet[UDP].dport, dport=packet[UDP].sport)/dns

            # Send the response
            try:
                send(dns_response, verbose=0)
                self._logger.debug(
                    f"dns.process_request: dns response sent {dns_response}")
            except Exception as error:
                return_state = self.ERR_EXCEPTION
                self._logger.exception(
                    f"dns.process_request: An unhandled error occurred \
{error}")
            self._logger.debug("dns.process_request: END")
            return return_state


# Unittest code
if "unittest" in sys.modules:
    class Testdns(unittest.TestCase):
        """Unittest module class"""

        def test_run(self):
            """Test the run method of dns class"""
            # Create a logger for the test
            logger = logging.getLogger("output/logs/testunits.log")

            # Create a dns instance
            server = dns(logger)

            # Check for correct exit codes
            self.assertEqual(server.OK, int(0))
            self.assertEqual(server.ERROR, int(1))
            self.assertEqual(server.ERR_TIMEOUT, int(2))
            self.assertEqual(server.ERR_EXCEPTION, int(255))

            # Tests that logger is correctly setup by __init__
            self.assertTrue(isinstance(
                server._logger, logging.Logger), "Bad __init__ value")

            # Tests that LAYER is correctly setup by __init__
            self.assertEqual(server.LAYER, int(7), "Bad __init__ value")

            # Tests that PRIORITY is correctly setup by __init__
            self.assertEqual(server.PRIORITY, int(1), "Bad __init__ value")

            # Tests that APPLICATION is correctly setup by __init__
            self.assertEqual(server.APPLICATION, "dns", "Bad __init__ value")

            # Call the run method for TCP and check the return value
            # as this is a simple test there shouldn't be a response
            # so a timeout is expected, as a good result for test
            dport = 553
            if os.geteuid() == 0:
                self.assertEqual(server.run(6, "192.168.0.1", "0.0.0.0", 2347,
                                            dport, timeout=1), server.OK)
            else:
                print("This test should be run as root")
                sys.exit(1)

            # Checks that values are properly set
            self.assertEqual(server._proto, int(6), "Bad IP protocol")

            self.assertTrue(
                isinstance(ipaddress.ip_address(server._ip),
                           ipaddress.IPv4Address), "Bad IP address")
            self.assertTrue(isinstance(server._srcport, int),
                            "Bad source port")
            self.assertTrue(isinstance(server._dstport, int),
                            "Bad destination port")

            # Now testing with bad protocol
            self.assertEqual(server.run(
                proto="proto", srcip="192.168.0.1", address="0.0.0.0",
                srcport=4567, dstport=dport), server.ERR_EXCEPTION,
                "Bad protocol accepted")

            # Now testing with bad source IP
            self.assertEqual(server.run(
                proto=6, srcip="ipaddr", address="0.0.0.0", srcport=4567,
                dstport=dport), server.ERR_EXCEPTION,
                "Bad source IP accepted")

            # Now testing with bad destination IP
            self.assertEqual(server.run(
                proto=6, srcip="192.168.0.1", address="ipaddr", srcport=4567,
                dstport=dport), server.ERR_EXCEPTION,
                "Bad destination IP accepted")

            # Now testing with bad source port
            self.assertEqual(server.run(
                proto=6, srcip="192.168.0.1", address="0.0.0.0",
                srcport="port", dstport=dport), server.ERR_EXCEPTION,
                "Bad source port accepted")

            # Now testing with bad destination port
            self.assertEqual(server.run(
                proto=6, srcip="192.168.0.1", address="0.0.0.0", srcport=4567,
                dstport="port"), server.ERR_EXCEPTION,
                "Bad destination port accepted")
