import socket
import sys


class tcp():
    # Exit codes
    OK = 0
    ERROR = 1
    ERR_TIMEOUT = 2
    ERR_EXCEPTION = 255

    LAYER = 3
    PROTOCOL_NUMBER = 6
    PRIORITY = 1

    def __init__(self, logger):
        self._logger = logger

    def get_service(self, service, numericformat=True):
        if numericformat:
            if service.lower() in ["dns"]:
                service = "domain"
            try:
                return socket.getservbyname(service.lower())
            except (ValueError, OSError):
                pass
        else:
            try:
                return socket.getservbyport(int(service))
            except (ValueError, OSError):
                pass
        return service

    def run(self, proto, srcip, ip, srcport, dstport, searchfor):
        """
        Waits for a tcp data and sends a response
        """

        self._logger.debug("tcp.run: BEGIN")
        socket.allow_reuse_address = True
        # Open a tcp socket
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM, int(proto))
        s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

        service = self.get_service(dstport)
        self._logger.debug(
            "tcp.run: Service number is {f1}".format(f1=repr(service)))
        try:
            # Bind socket to server ip and port
            s.bind((ip, int(service)))

            self._logger.debug("tcp.run: Socket bind complete")
            print('Socket bind complete')
            self._logger.debug("listen: Socket bind complete")
        except socket.error as msg:
            if msg is list and len(msg) == 2:
                print(
                    'Bind failed. Error Code : '+str(msg[0])+' Message'+msg[1])
                self._logger.debug(
                    'tcp.run: Bind failed. Error Code : '+str(msg[0]) +
                    ' Message '+msg[1])
            else:
                print('listen: Bind failed: {f1}'.format(f1=repr(msg)))
                self._logger.debug(
                    'tcp.run: Bind failed: {f1}'.format(f1=repr(msg)))
            sys.exit(self.ERROR)

        sockopen = False
        connopen = False
        try:
            # Start listening on socket
            s.listen(0)
            sockopen = True
            print('Server now listening')
            self._logger.debug("tcp.run: Server now listening")

            # Waits to accept a connection - blocking call
            conn, addr = s.accept()
            connopen = True

            self._logger.debug(
                'tcp.run: Connected with {f1}:{f2}'.format(
                    f1=repr(addr[0]), f2=repr(addr[1])))
            self._logger.debug(
                'tcp.run: Connected with '+addr[0]+':'+str(addr[1]))

            # infinite loop so that function do not terminate and thread
            # do not end.
            received = ""
            while True:
                # Receiving from client
                max_data_size = 1024
                data = conn.recv(max_data_size).decode("utf-8")

                received = received+data
                self._logger.debug("tcp.run: Received {f1}".format(
                    f1=repr(received)))
                if not data:
                    break

                if str(received).replace("\n", "") == str(searchfor):
                    conn.send(searchfor.encode("utf-8"))

                    self._logger.debug(
                        "tcp.run: Sended back {f1}".format(f1=repr(searchfor)))
                    break
        except Exception as e:
            print(e)

        finally:
            # Close server socket
            if connopen:
                conn.shutdown(1)
                conn.close()

            if sockopen:
                s.shutdown(1)
                s.close()
        self._logger.debug("tcp.run: END")
        return self.OK
