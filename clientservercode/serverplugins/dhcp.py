"""This plugin waits for a dhcp discover packet
and returns a dhcp offer.
It's the server side plugin for dhcp tester client one.
"""

import ipaddress
import logging
import sys
if "unittest" in sys.modules:
    import os
    import unittest

from scapy.sendrecv import sniff
from scapy.all import DHCP, Ether, IP, UDP, BOOTP, sendp, Scapy_Exception


class dhcp:
    """This is the dhcp plugin server side class.

    This plugin is intended to run at layer 7 for dhcp protocol.
    It's composed by 2 methods:
    __init__: The standard init method which receives a logger object.
    run: The method which will be called by Client.py to run the
         plugin code.

    This plugin use the following constants
        LAYER = 7 (Applications layer)
        PRIORITY = 1 (first plugin for DHCP)
        APPLICATION = "dhcp" (Application name for the plugin)
    And these exit codes:
        OK = 0
        ERROR = 1
        ERR_TIMEOUT = 2
        ERR_EXCEPTION = 255
    """

    # Exit codes
    OK = 0
    ERROR = 1
    ERR_TIMEOUT = 2
    ERR_EXCEPTION = 255

    LAYER = 7
    PRIORITY = 1
    APPLICATION = "dhcp"

    def __init__(self, logger):
        """This method inizializes the logger and local variables.

        Args:
            logger (logging.logger): Logger object to use
            to write log messages. Defaults to None.
        """

        self._logger = logger
        self._proto = self._ip = self._srcport = self._dstport = None

    def run(self, proto=17, srcip="0.0.0.0", address="255.255.255.255",
            srcport=68, dstport=67, searchfor=None, timeout=None):
        """This function generate an standard dhcp offer packet.

        Wait for a dhcp dicover packet and sends back a dhcp offer and exit.
        This method ensures that the received parameters are RFC
        compliance, if aren't, sets them to the RFC values. So you can't
        use this plugin to test non standard DHCP scenarios.

        Args:
            proto (int, optional): The layer 3 protcolo to use.
                Must be 17 (UDP).
            address (str, optional): Server IP address.
                Defaults to multicast address "255.255.255.255".
            srcport (int, optional): Layer 3 source port.
                Defaults and must be 68.
            dstport (int, optional): Layer 3 destination port.
                Defaults and must be 67.
            searchfor (str, optional): Not used by the module.
                Defaults to None.
        """

        return_state = self.OK
        try:
            # Logs begin of function
            self._logger.debug("dhcp.run: BEGIN")

            # Store ip in a private object property
            self._ip = str(address)
            try:
                address = ipaddress.ip_address(address)
            except ValueError:
                self._logger.warning(
                    "dhcp.run: IP address is not valid set it to \
multicast 255.255.255.255")
                self._ip = "255.255.255.255"

            # Check if protocol is udp
            if proto != int(17):
                self._logger.warning(
                    "dhcp.run: protocol does not follows RFC set it to \
udp(17)")
                proto = 17
            self._proto = int(proto)

            # Check if srcport is integer(68)
            # if is not set it to 68 following the DHCP's RFC
            if srcport != int(68):
                self._logger.warning(
                    "dhcp.run: srcport does not follows RFC set it to 68")
                srcport = 68
            self._srcport = int(srcport)

            # Check if dstport is integer(67)
            # if is not set it to 67 following the DHCP's RFC
            if dstport != int(67):
                self._logger.debug(
                    "dhcp.run: dstport does not follows RFC set it to 67"
                )
                dstport = 67
            self._dstport = int(dstport)

            # Create a DHCP request filter
            dhcp_request_filter = f"udp and port {self._dstport}"

            # Start sniffing for DHCP requests
            self._logger.debug(
                f"dhcp.run: Listening request protocol: {self._proto} \
to: {self._ip} srcport: {self._srcport} dstport: {self._dstport}")
            sniff(prn=self.process_request,
                  filter=dhcp_request_filter, store=0, count=1,
                  timeout=timeout)
        except ValueError as error:
            self._logger.exception(
                f"dhcp.run: A ValueError occurred: {error}")
            return_state = self.ERR_EXCEPTION
        except OSError as error:
            self._logger.exception(
                f"dhcp.run: An OSError occurred: {error}")
            return_state = self.ERR_EXCEPTION
        except TypeError as error:
            self._logger.exception(
                f"dhcp.run: A TypeError occurred: {error}")
            return_state = self.ERR_EXCEPTION
        except Scapy_Exception as error:
            self._logger.exception(
                f"dhcp.run: An Scapy exception occurred: {error}")
            return_state = self.ERR_EXCEPTION
        except Exception as error:
            self._logger.exception(
                f"dhcp.run: An unhandled error occurred: {error}")
            return_state = self.ERR_EXCEPTION
        self._logger.debug("dhcp.run: END")
        return return_state

    def process_request(self, packet):
        self._logger.debug("dhcp.process_request: BEGIN")
        # Check if the packet is a DHCP request
        return_state = self.OK
        if packet[DHCP]:
            # Extract the client IP and MAC address
            client_ip = packet[IPe].src
            client_mac = packet[Ether].src

            # self._logger.debug the request information
            self._logger.debug(
                f"dhcp.process_request: DHCP request received from IP \
{client_ip} and MAC {client_mac}")

            # Create a DHCP response
            dhcp_response = Ether(dst=client_mac, src=packet[Ether].dst)/IP(
                src=packet[IP].dst, dst=client_ip)/UDP(
                    sport=packet[UDP].dport, dport=packet[UDP].sport)/BOOTP(
                    op=2, yiaddr=client_ip, siaddr=packet[IP].dst,
                    chaddr=client_mac)/DHCP(
                    options=[("message-type", "offer"), "end"])

            # Send the response
            try:
                sendp(dhcp_response, verbose=0)
                self._logger.debug(
                    f"dhcp.process_request: DHCP response sent {client_ip}")
            except Exception as error:
                return_state = self.ERR_EXCEPTION
                self._logger.exception(
                    f"dhcp.process_request: An unhandled error occurred \
{error}")
            self._logger.debug("dhcp.process_request: END")
            return return_state


# Unittest code
if "unittest" in sys.modules:
    class Testdhcp(unittest.TestCase):
        """Unittest module class"""

        def test_run(self):
            """Test the run method of dhcp class"""

            # Create a logger for the test
            logger = logging.getLogger("output/logs/testunits.log")

            # Create a dhcp instance
            server = dhcp(logger)

            # Check for correct exit codes
            self.assertEqual(server.OK, int(0))
            self.assertEqual(server.ERROR, int(1))
            self.assertEqual(server.ERR_TIMEOUT, int(2))
            self.assertEqual(server.ERR_EXCEPTION, int(255))

            # Tests that logger is correctly setup by __init__
            self.assertTrue(isinstance(
                server._logger, logging.Logger), "Bad __init__ value for")

            # Tests that LAYER is correctly setup by __init__
            self.assertEqual(server.LAYER, int(7), "Bad __init__ value")

            # Tests that LAYER is correctly setup by __init__
            self.assertEqual(server.PRIORITY, int(1), "Bad __init__ value")

            # Tests that LAYER is correctly setup by __init__
            self.assertEqual(server.APPLICATION, "dhcp", "Bad __init__ value")

            # Call the run method and check the return value
            # as this is a simple test ther shouldn't be a response
            # so a timeout is expected, and a good result for test
            # the test also set default values for params
            if os.geteuid() == 0:
                self.assertEqual(server.run(timeout=1), server.OK)
            else:
                print("This test should be run as root")
                sys.exit(1)

            # # Checks that default values are properly set
            self.assertEqual(server._proto, int(17), "Bad default value")
            self.assertEqual(
                server._ip, "255.255.255.255", "Bad default value")
            self.assertEqual(server._srcport, int(68), "Bad default value")
            self.assertEqual(server._dstport, int(67), "Bad default value")

            # Now testing with bad params to check sanitization
            self.assertEqual(server.run(
                proto="proto", address="ip", srcport="srcport",
                dstport="dstport", timeout=1), server.OK)

            # Checks that default values are properly set
            self.assertEqual(server._proto, int(17), "Fuzzed value accepted")
            self.assertEqual(
                server._ip, "255.255.255.255", "Fuzzed value accepted")
            self.assertEqual(server._srcport, int(68), "Fuzzed value accepted")
            self.assertEqual(server._dstport, int(67), "Fuzzed value accepted")
