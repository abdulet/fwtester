import socket
import sys


class udp():
    # Exit codes
    OK = 0
    ERROR = 1
    ERR_TIMEOUT = 2
    ERR_EXCEPTION = 255

    LAYER = 3
    PROTOCOL_NUMBER = 17
    PRIORITY = 1

    def __init__(self, logger):
        self._logger = logger

    def _get_service(self, service, numericformat=True):
        if numericformat:
            if service.lower() in ["dns"]:
                service = "domain"
            try:
                return socket.getservbyname(service.lower())
            except (ValueError, OSError):
                pass
        else:
            try:
                return socket.getservbyport(int(service))
            except (ValueError, OSError):
                pass
        return service

    def run(self, proto, srcip, ip, srcport, dstport, searchfor):
        """
        Waits for a udp data and sends a response
        """
        self._logger.debug("udp.run: BEGIN")
        socket.allow_reuse_address = True
        # Open a udp socket
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, int(proto))
        s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        service = self._get_service(dstport)
        self._logger.debug(
            "udp.run: Service number is {f1} and proto {f2}".format(
                f1=repr(service), f2=repr(proto)))
        try:
            # Bind socket to server ip and port
            s.bind((ip, int(service)))
            self._logger.debug("udp.run: Socket bind complete")
        except socket.error as msg:
            if msg is list and len(msg) == 2:
                self._logger.debug(
                    'udp.run: Bind failed. Error Code : ' + str(msg[0])
                    + ' Message '+msg[1])
            else:
                self._logger.debug('udp.run: Bind failed: {f1}'.format(
                    f1=repr(msg)))
                self._logger.debug(
                    '  Data is: {f1}'.format(
                        f1=repr([proto, ip, srcport, dstport])))
            sys.exit(self.ERROR)
        sockopen = False
        connopen = False
        try:
            print('Server now listening')
            self._logger.debug("udp.run: Server now listening")

            # Sending message to connected client
            # conn.send('Welcome to the server.Type something and hit enter\n')
            # send only takes string

            # infinite loop so that function do not terminate and
            # thread do not end.
            received = ""
            while True:
                # Receiving from client
                max_data_size = 1024
                data, addr = s.recvfrom(max_data_size)
                data = data.decode("utf-8")

                received = received+data
                self._logger.debug("udp.run: rec {f1}".format(
                    f1=repr(received)))
                if not data:
                    break

                self._logger.debug("udp.run: Received {f1}".format(
                    f1=repr(received)))

                if str(received).replace("\n", "") == str(searchfor):
                    s.sendto(searchfor.encode("utf-8"), addr)

                    self._logger.debug(
                        "udp.run: Sended back: {f1}".format(
                            f1=repr(searchfor)))
                    break
        except Exception as e:
            print(e)

        finally:
            # Close server socket
            # if connopen:
            #     conn.shutdown(1)
            #     conn.close()

            if sockopen:
                s.shutdown(1)
                s.close()
        self._logger.debug("udp.run: END")
        return self.OK
