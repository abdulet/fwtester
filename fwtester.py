#!/usr/bin/env /usr/bin/python3
# -*- coding: utf-8 -*-

from classes.FwTester import FwTester

if __name__ == "__main__":
    tester = FwTester("config/config.yml")
    tester.run()
