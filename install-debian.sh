#!/bin/bash
# Download this script from: git clone https://gitlab.com/abdulet/fwtester.git
# This script is in a "work in progress state" not fully working

if ! which apt
then
    echo "apt not found. This script is only for debian based distros."
    exit 1
fi

if ! id | grep root > /dev/null
then
    echo "Please run this script as root."
    exit 1
fi

apt -y install sudo vim git  python3-fake-factory python3-pexpect python3-pymongo python3-scapy python3-yaml python3-lxc net-tools snapd gnupg2 bridge-utils acpid
apt -y purge nano
snap install lxd
sed -i 's/GRUB_CMDLINE_LINUX=""/GRUB_CMDLINE_LINUX="net.ifnames=0"/' /etc/default/grub
sed -i "s/allow-hotplug .*/allow-hotplug eth0/" /etc/network/interfaces
sed -i "s/iface .* inet dhcp/iface eth0 inet dhcp/" /etc/network/interfaces
update-grub
useradd --groups sudo,lxd --create-home --shell /bin/bash --user-group fwtester
echo fwtester:fwt3st3r | chpasswd
cat >> /etc/network/interfaces.d/fwtbr0 <<EOF
#lxd bridge interface
auto eth1 fwtbr0
iface eth1 inet manual
iface fwtbr0 inet manual
  bridge_ports eth1
  bridge_stp off
  bridge_fd 0
  bridge_maxwait 0
EOF
wget -qO - https://www.mongodb.org/static/pgp/server-5.0.asc | apt-key add -
echo "deb http://repo.mongodb.org/apt/debian buster/mongodb-org/5.0 main" | tee /etc/apt/sources.list.d/mongodb-org-5.0.list
apt update
apt -y install mongodb-org
service mongod start
systemctl enable --now mongod
/snap/bin/lxd init --auto
su - fwtester -c "/snap/bin/lxc launch images:debian/11 server"
su - fwtester -c "/snap/bin/lxc launch images:debian/11 client"
su - fwtester -c "/snap/bin/lxc launch images:debian/11 router"
su - fwtester -c "/snap/bin/lxc exec server -- apt -y install python3 python3-scapy tcpdump && apt clean"
su - fwtester -c "/snap/bin/lxc exec server mkdir /root/serverplugins"
su - fwtester -c "/snap/bin/lxc exec server -- ln -s /usr/bin/python3 /usr/bin/python"
su - fwtester -c "/snap/bin/lxc exec client -- apt -y install python3 python3-scapy tcpdump && apt clean"
su - fwtester -c "/snap/bin/lxc exec client mkdir /root/clientplugins/"
su - fwtester -c "/snap/bin/lxc exec client  -- ln -s /usr/bin/python3 /usr/bin/python"
su - fwtester -c "/snap/bin/lxc exec router -- apt -y install tcpdump && apt clean"
su - fwtester -c "/snap/bin/lxc exec router -- bash -c 'cat >> /etc/iproute2/rt_tables << EOF
2       server
1       client
EOF'"
ifup enp0s8
ifup fwtbr0
su - fwtester -c "/snap/bin/lxc profile device remove default eth0"
su - fwtester -c "/snap/bin/lxc profile device add default eth0 nic nictype=bridged parent=fwtbr0"
su - fwtester -c "/snap/bin/lxc restart server client router"
su - fwtester -c "git clone https://gitlab.com/abdulet/fwtester.git"
cat > fwtester.sh << EOF
#!/bin/bash
cd /home/fwtester/fwtester
./fwtester.py
EOF
chmod +x fwtester.sh
mv fwtester.sh /usr/local/bin/fwtester
apt clean
reboot
